package main

import (
	"bufio"
	"fmt"
	"os"
	"path/filepath"
)

var path string
var relative = false

func main() {

	//Check if there is an argument provided
	if len(os.Args) > 1 {
		path = os.Args[1]
	} else {

		// Get the path from the environment variable
		path = os.Getenv("NOTAS_PATH")
	}

	if path == "" {

		relative = true

		var err error
		path, err = os.Getwd()
		if err != nil {
			fmt.Printf("Error getting current directory: %v\n", err)
			return
		}
	}

	// Traverse the directory recursively
	err := filepath.Walk(path, func(path string, info os.FileInfo, err error) error {
		/*
			if err != nil {
				return err
			}
		*/

		// Check if the file has a ".md" extension
		if filepath.Ext(path) == ".md" {
			processFile(path)
		}
		return nil
	})

	if err != nil {
		fmt.Printf("Error walking the path: %v\n", err)
	}
}

func processFile(path string) {
	file, err := os.Open(path)
	if err != nil {
		fmt.Printf("Error opening file %s: %v\n", path, err)
		return
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		if len(line) > 0 && line[0] == '#' && line[1] == ' ' {
			truncatedLine := truncateLine(line, 40)
			wd, err := os.Getwd()
			if err != nil {
				fmt.Printf("Error getting current directory: %v\n", err)
				return
			}

			if relative {
				relativePath, err := filepath.Rel(wd, path)
				if err != nil {
					fmt.Printf("Error getting relative path: %v\n", err)
					return
				}
				fmt.Printf("%-60s | %s\n", truncatedLine, relativePath)
			} else {

				fmt.Printf("%-60s | %s\n", truncatedLine, path)
			}

			break
		}
	}

	if err := scanner.Err(); err != nil {
		fmt.Printf("Error reading file %s: %v\n", path, err)
	}
}

func truncateLine(line string, length int) string {
	yellow := "\033[33m"
	reset := "\033[0m"
	if len(line) > length {
		line = line[:length] + "..."
	} // Color the pound sign green and the rest of the line yellow
	coloredLine := fmt.Sprintf("%s%s%s%s", yellow, string(line[0]), reset, line[1:])
	return coloredLine
}
