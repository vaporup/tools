#!/usr/bin/env bash

for tool in $(grep 'https:/' toolothek/todo.txt | awk '{print $1}' | awk -F'/' '{print $NF}'); do
  sleep 10
  curl -s "https://packages.ubuntu.com/noble/$tool" \
  | grep -q 'No such package'    \
  && echo "https://packages.ubuntu.com/$tool"
done
