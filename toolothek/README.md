<h1 align="center"><img src="logo.png" height="80"/></h1>
<p align="center">Search your own database of tools</p>

## Usage

```
Usage: toolothek [OPTIONS] | QUERY

Options:

  -h  --help	show this help message
  -u  --update	update sqlite index
  -t  --tags	list tags

Examples:

  toolothek '*'
  toolothek 'ssh'
  toolothek 'name: ^ssh'
  toolothek 'description: collection'
  toolothek 'tag: multi* OR tag: game'
```

## Searching

```
toolothek 'ssh'
```

```
putty                          : SSH client for X
putty-tools                    : command-line tools for SSH, SCP, and SFTP
ssh-askpass                    : under X, asks user for a passphrase for ssh-add
ssh-chat                       : chat over SSH
ssh-tools                      : collection of various tools using ssh
sshpass                        : Non-interactive ssh password authentication
```

## Similar Projects

- https://github.com/toolleeo/cli-apps
- https://robot.unipv.it/clipedia/about/
- https://terminaltrove.com
- https://www.trackawesomelist.com

## Related

- https://debaday.debian.net (not updated anymore)
  - https://debaday.debian.net/about-this-site/
  - https://www.wired.com/2008/04/debaday-project-needs-your-help/
