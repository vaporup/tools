package main

import (
	"bufio"
	"database/sql"
	"fmt"
	//_ "github.com/mattn/go-sqlite3"
	_ "modernc.org/sqlite"
	"io/ioutil"
	"log"
	"os"
	"path"
	"path/filepath"
	"regexp"
	"strings"
)

var (
	Info = Teal
	Warn = Yellow
	Fata = Red
)

var (
	Black   = Color("\033[30m%s\033[0m")
	Red     = Color("\033[31m%s\033[0m")
	Green   = Color("\033[32m%s\033[0m")
	Yellow  = Color("\033[33m%s\033[0m")
	Purple  = Color("\033[34m%s\033[0m")
	Magenta = Color("\033[35m%s\033[0m")
	Teal    = Color("\033[36m%s\033[0m")
	White   = Color("\033[37m%s\033[0m")
)

func Color(colorString string) func(...interface{}) string {
	sprint := func(args ...interface{}) string {
		return fmt.Sprintf(colorString,
			fmt.Sprint(args...))
	}

	return sprint
}

func get_executable_info() (string, string) {

	filepath, err := os.Executable()

	if err != nil {
		log.Println(err)
	}

	return path.Base(filepath), path.Dir(filepath)

}

func showUsage() {

	executable_name, _ := get_executable_info()

	fmt.Println("\n\tUsage:", executable_name, "[OPTIONS] | QUERY")

	fmt.Println("\n\tOptions:\n")

	fmt.Println("\t  -h  --help\tshow this help message")
	fmt.Println("\t  -u  --update\tupdate sqlite index")
	fmt.Println("\t  -t  --tags\tlist tags")
	fmt.Println("\t  -s  --show\tshow info of a specific tool")

	fmt.Println("\n\tExamples:\n")

	fmt.Println("\t ", executable_name, "'*'")
	fmt.Println("\t ", executable_name, "'ssh'")
	fmt.Println("\t ", executable_name, "'name: ^ssh'")
	fmt.Println("\t ", executable_name, "'description: collection'")
	fmt.Println("\t ", executable_name, "'tag: multi* OR tag: game'")

}

func main() {

	if len(os.Args) == 1 {

		fmt.Println(Warn("\n\tNo query given"))

		showUsage()
		os.Exit(0)
	}

	if os.Args[1] == "-h" || os.Args[1] == "--help" {
		showUsage()
		os.Exit(0)
	}

	if os.Args[1] == "-u" || os.Args[1] == "--update" {

		var allTags = map[string]string{}
		var counter int

		os.Remove("./tools.db")

		db, err := sql.Open("sqlite", "./tools.db")

		if err != nil {
			log.Fatal(err)
		}

		defer db.Close()

		sqlStmt := `
                CREATE VIRTUAL table tools USING fts5(name, description, tag);
                CREATE VIRTUAL table tags USING fts5(name);
                `

		_, err = db.Exec(sqlStmt)

		if err != nil {
			log.Printf("%q: %s\n", err, sqlStmt)
			return
		}

		_, executable_path := get_executable_info()

		data := filepath.Join(executable_path, "data")

		if strings.Contains(executable_path, "go-build") {
			data = filepath.Join("./", "data")
		}

		entries, err := os.ReadDir(data)

		if err != nil {
			log.Fatal(err)
		}

		for _, entry := range entries {

			if entry.IsDir() {

				info, _ := entry.Info()
				dirname := filepath.Join(data, info.Name())

				subentries, err := os.ReadDir(dirname)

				if err != nil {
					log.Fatal(err)
				}

				for _, subentry := range subentries {

					if subentry.IsDir() {

						subinfo, _ := subentry.Info()
						subdirname := filepath.Join(dirname, subinfo.Name())
						readme := filepath.Join(subdirname, "README.md")

                                                //fmt.Println(readme)

						fh, err := os.Open(readme)

						if err != nil {
							log.Fatal(err)
						}

						fileScanner := bufio.NewScanner(fh)
						fileScanner.Split(bufio.ScanLines)

						var name string
						var desc string
						var tags []string

						for fileScanner.Scan() {
							line := fileScanner.Text()
							emptyLine, _ := regexp.MatchString(`^\s*$`, line)
							dataLine, _ := regexp.MatchString(`^\s{4}`, line)

							if emptyLine {
								continue
							}

							if !dataLine {
								continue
							}

							rawKey := strings.SplitN(line, ":", 2)[0]
							rawVal := strings.SplitN(line, ":", 2)[1]

							key := strings.TrimSpace(rawKey)
							val := strings.TrimSpace(rawVal)

							if key == "name" {
								name = val

							}

							if key == "description" {
								desc = val

							}

							if key == "tag" {
								tags = append(tags, val)
								allTags[val] = "1"

							}

						}

						counter = counter + 1
						fmt.Printf("Indexing (%v)\r", counter)
						foo := fmt.Sprintf("INSERT INTO tools(name, description, tag) VALUES('%s', '%s', '%s');", name, desc, strings.Join(tags, " "))

						_, err = db.Exec(foo)

						if err != nil {
							log.Fatal(err)
						}

					}
				}
			}
		}

		for tag, _ := range allTags {
			stmt := fmt.Sprintf("INSERT INTO tags(name) VALUES('%s');", tag)

			_, err = db.Exec(stmt)

			if err != nil {
				log.Printf("%q: %s\n", err, stmt)
				return
			}

		}
		os.Exit(0)

	}

	if os.Args[1] == "-t" || os.Args[1] == "--tags" {

		db, err := sql.Open("sqlite", "./tools.db")

		if err != nil {
			log.Fatal(err)
		}

		defer db.Close()

		rows, err := db.Query("SELECT name FROM tags ORDER BY name")
		if err != nil {
			log.Fatal(err)
		}
		defer rows.Close()
		for rows.Next() {
			var name string
			err = rows.Scan(&name)
			if err != nil {
				log.Fatal(err)
			}
			fmt.Println(name)
		}
		err = rows.Err()
		if err != nil {
			log.Fatal(err)
		}

		os.Exit(0)
	}

	if os.Args[1] == "-s" || os.Args[1] == "--show" {

		toolname := os.Args[2]
		toolprefix := toolname[0:1]
		toolfilepath := fmt.Sprintf("data/%v/%v/README.md", toolprefix, toolname)

		content, err := ioutil.ReadFile(toolfilepath)

		if err != nil {
			fmt.Println("FileNotFound: ", toolfilepath)
		}

		fmt.Println()
		fmt.Print(string(content))

		os.Exit(0)
	}

	if os.Args[1] == "*" {

		db, err := sql.Open("sqlite", "./tools.db")

		if err != nil {
			log.Fatal(err)
		}

		defer db.Close()

		rows, err := db.Query("SELECT name, description FROM tools")
		if err != nil {
			log.Fatal(err)
		}
		defer rows.Close()
		for rows.Next() {
			var name string
			var desc string
			err = rows.Scan(&name, &desc)
			if err != nil {
				log.Fatal(err)
			}
			fmt.Printf("%-*v : %-v\n", 30, name, desc)
		}
		err = rows.Err()
		if err != nil {
			log.Fatal(err)
		}

		os.Exit(0)

	} else {
		db, err := sql.Open("sqlite", "./tools.db")

		if err != nil {
			log.Fatal(err)
		}

		defer db.Close()

		foo3 := fmt.Sprintf("SELECT name, description FROM tools WHERE tools = '%s';", os.Args[1])

		rows, err := db.Query(foo3)

		if err != nil {
			log.Fatal(err)
		}
		defer rows.Close()
		for rows.Next() {
			var name string
			var desc string
			err = rows.Scan(&name, &desc)
			if err != nil {
				log.Fatal(err)
			}
			fmt.Printf("%-*v : %-v\n", 30, name, desc)
		}
		err = rows.Err()
		if err != nil {
			log.Fatal(err)
		}

		os.Exit(0)

	}

}
