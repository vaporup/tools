#!/usr/bin/env bash

find -mindepth 2 -name README.md -exec bash -c "echo ; cat {}" \;
