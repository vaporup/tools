#!/usr/bin/env bash

find -mindepth 2 -name README.md -exec bash -c "echo ; cat {}" \; \
| sed -e 's/^[[:space:]]*//' -e  's/[[:space:]]*:/:/' \
| recsel
