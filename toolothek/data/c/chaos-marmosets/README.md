    name        : chaos-marmosets
    description : small chaos monkey programs
    homepage    : https://github.com/bdrung/chaos-marmosets
    repology    : https://repology.org/project/chaos-marmosets/versions
    pkgs_ubuntu : https://packages.ubuntu.com/chaos-marmosets
    pkgs_debian : https://packages.debian.org/chaos-marmosets
