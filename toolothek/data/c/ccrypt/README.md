    name        : ccrypt
    description : secure encryption and decryption of files and streams
    homepage    : http://ccrypt.sourceforge.net
    pkgs_ubuntu : https://packages.ubuntu.com/ccrypt
    pkgs_debian : https://packages.debian.org/ccrypt
