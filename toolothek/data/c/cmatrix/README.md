    name        : cmatrix
    description : simulates the display from "The Matrix"
    homepage    : https://github.com/abishekvashok/cmatrix
    pkgs_ubuntu : https://packages.ubuntu.com/cmatrix
    pkgs_debian : https://packages.debian.org/cmatrix
