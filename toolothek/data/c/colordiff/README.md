    name        : colordiff
    description : colorize diff output
    homepage    : https://www.colordiff.org
    pkgs_ubuntu : https://packages.ubuntu.com/colordiff
    pkgs_debian : https://packages.debian.org/colordiff
