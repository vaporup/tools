    name        : clsync
    description : live sync tool based on inotify
    homepage    : https://ut.mephi.ru/oss/clsync
    repology    : https://repology.org/project/clsync/versions
    pkgs_ubuntu : https://packages.ubuntu.com/clsync
    pkgs_debian : https://packages.debian.org/clsync
