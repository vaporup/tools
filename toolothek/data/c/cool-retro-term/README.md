    name        : cool-retro-term
    description : terminal emulator which mimics old screens
    homepage    : https://github.com/Swordfish90/cool-retro-term
    pkgs_ubuntu : https://packages.ubuntu.com/cool-retro-term
    pkgs_debian : https://packages.debian.org/cool-retro-term
