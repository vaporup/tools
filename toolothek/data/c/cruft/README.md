    name        : cruft
    description : finds any cruft built up on your system
    homepage    : https://github.com/a-detiste/cruft
    pkgs_ubuntu : https://packages.ubuntu.com/cruft
    pkgs_debian : https://packages.debian.org/cruft
