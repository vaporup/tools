    name        : cclive
    description : lightweight command line video extraction tool
    homepage    : http://cclive.sourceforge.net
    pkgs_ubuntu : https://packages.ubuntu.com/cclive
    pkgs_debian : https://packages.debian.org/cclive
