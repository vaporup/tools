    name        : ccextractor
    description : fast closed captions extractor for MPEG and H264 files
    homepage    : https://www.ccextractor.orgo
    repology    : https://repology.org/project/ccextractor/versions
    pkgs_ubuntu : https://packages.ubuntu.com/ccextractor
    pkgs_debian : https://packages.debian.org/ccextractor
    tag         : subtitle
