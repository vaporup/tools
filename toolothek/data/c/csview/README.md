    name        : csview
    description : high performance csv viewer with cjk/emoji support
    homepage    : https://github.com/wfxr/csview
