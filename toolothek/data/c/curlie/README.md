    name        : curlie
    description : The power of curl, the ease of use of httpie
    homepage    : https://github.com/rs/curlie
    pkgs_vaporup: http://apt.vaporup.de/pool/main/c/curlie/
