    name        : cheat
    description : interactive cheatsheets on the command-line
    homepage    : https://github.com/cheat/cheat
    pkgs_vaporup: http://apt.vaporup.de/pool/main/c/cheat/
