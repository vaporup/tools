    name        : cdpr
    description : Cisco Discovery Protocol Reporter
    homepage    : https://sourceforge.net/projects/cdpr/
    pkgs_ubuntu : https://packages.ubuntu.com/cdpr
    pkgs_debian : https://packages.debian.org/cdpr
