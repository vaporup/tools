    name        : calcurse
    description : text-based calendar and todo manager
    homepage    : https://calcurse.org
    pkgs_ubuntu : https://packages.ubuntu.com/calcurse
    pkgs_debian : https://packages.debian.org/calcurse
