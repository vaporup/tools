    name        : cbm
    description : display the current network traffic in colors
    homepage    : https://github.com/resurrecting-open-source-projects/cbm
    pkgs_ubuntu : https://packages.ubuntu.com/cbm
    pkgs_debian : https://packages.debian.org/cbm
