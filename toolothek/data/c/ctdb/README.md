    name        : ctdb
    description : clustered database to store temporary data
    homepage    : https://ctdb.samba.org
    pkgs_ubuntu : https://packages.ubuntu.com/ctdb
    pkgs_debian : https://packages.debian.org/ctdb
