    name        : choose
    description : human-friendly alternative to cut and (sometimes) awk
    homepage    : https://github.com/theryangeary/choose
    pkgs_vaporup: http://apt.vaporup.de/pool/main/c/choose/
