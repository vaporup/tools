    name        : certigo
    description : examine and validate certificates in a variety of formats
    homepage    : https://github.com/square/certigo
    pkgs_vaporup: http://apt.vaporup.de/pool/main/c/certigo/
