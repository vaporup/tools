    name        : catimg
    description : image printing in your terminal
    homepage    : https://posva.net/shell/retro/bash/2013/05/27/catimg
    github      : https://github.com/posva/catimg
    pkgs_ubuntu : https://packages.ubuntu.com/catimg
    pkgs_debian : https://packages.debian.org/catimg
    video       : https://www.youtube.com/watch?v=Cy0ifA9ewmk
