    name        : cruft-ng
    description : finds any cruft built up on your system
    homepage    : https://github.com/a-detiste/cruft-ng/
    pkgs_ubuntu : https://packages.ubuntu.com/cruft-ng
    pkgs_debian : https://packages.debian.org/cruft-ng
