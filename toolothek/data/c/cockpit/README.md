    name        : cockpit
    description : enables users to administer GNU/Linux servers using a web browser
    homepage    : https://cockpit-project.org
    repology    : https://repology.org/project/cockpit/versions
    pkgs_ubuntu : https://packages.ubuntu.com/cockpit
    pkgs_debian : https://packages.debian.org/cockpit
