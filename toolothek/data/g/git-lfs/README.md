    name        : git-lfs
    description : Git Large File Support
    homepage    : https://git-lfs.github.com/
    repology    : https://repology.org/project/git-lfs/versions
    pkgs_ubuntu : https://packages.ubuntu.com/git-lfs
    pkgs_debian : https://packages.debian.org/git-lfs
