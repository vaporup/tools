    name        : gti
    description : a silly git launcher, inspired by sl
    homepage    : https://github.com/rwos/gti
    repology    : https://repology.org/project/gti/versions
    pkgs_vaporup: http://apt.vaporup.de/pool/main/g/gti/
