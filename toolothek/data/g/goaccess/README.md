    name        : goaccess
    description : real-time web log analyzer and interactive viewer
    homepage    : https://goaccess.io
    repology    : https://repology.org/project/goaccess/versions
    pkgs_ubuntu : http://packages.ubuntu.com/goaccess
    pkgs_debian : http://packages.debian.org/goaccess
