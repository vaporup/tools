    name        : gaupol
    description : subtitle editor for text-based subtitle files
    homepage    : https://otsaloma.io/gaupol/
    repology    : https://repology.org/project/gaupol/versions
    pkgs_ubuntu : https://packages.ubuntu.com/gaupol
    pkgs_debian : https://packages.debian.org/gaupol
