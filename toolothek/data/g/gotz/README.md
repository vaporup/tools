    name        : gotz
    description : CLI timezone info
    homepage    : https://github.com/merschformann/gotz
    repology    : https://repology.org/project/gotz/versions
    pkgs_vaporup: https://codeberg.org/vaporup/-/packages/debian/gotz/
