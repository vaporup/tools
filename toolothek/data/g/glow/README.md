    name        : glow
    description : Render markdown on the CLI, with pizzazz
    homepage    : https://github.com/charmbracelet/glow
    repology    : https://repology.org/project/glow/versions
