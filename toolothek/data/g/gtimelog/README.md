    name        : gtimelog
    description : time logging application
    homepage    : https://gtimelog.org
    repology    : https://repology.org/project/gtimelog/versions
    pkgs_ubuntu : https://packages.ubuntu.com/gtimelog
    pkgs_debian : https://packages.debian.org/gtimelog
