    name        : gbt
    description : configurable prompt builder for Bash, ZSH and PowerShell
    homepage    : https://github.com/jtyr/gbt
    repology    : https://repology.org/project/gbt/versions
