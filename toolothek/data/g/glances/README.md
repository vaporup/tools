    name        : glances
    description : curses based monitoring tool
    homepage    : https://nicolargo.github.io/glances
    repology    : https://repology.org/project/glances/versions
    github      : https://github.com/nicolargo/glances
    pkgs_ubuntu : https://packages.ubuntu.com/glances
    pkgs_debian : https://packages.debian.org/glances
    video       : https://www.youtube.com/watch?v=DO06do9N-pw
