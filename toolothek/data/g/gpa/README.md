    name        : gpa
    description : GNU Privacy Assistant
    homepage    : https://gnupg.org/software/gpa
    repology    : https://repology.org/project/gpa/versions
    pkgs_ubuntu : https://packages.ubuntu.com/gpa
    pkgs_debian : https://packages.debian.org/gpa
