    name        : gdu
    description : Pretty fast disk usage analyzer
    homepage    : https://github.com/dundee/gdu
    repology    : https://repology.org/project/gdu/versions
    pkgs_ubuntu : https://packages.ubuntu.com/gdu
    pkgs_debian : https://packages.debian.org/gdu
    pkgs_vaporup: http://apt.vaporup.de/pool/main/g/gdu/
