    name        : git-sizer
    description : compute various size metrics for a Git repository
    homepage    : https://github.com/github/git-sizer
    repology    : https://repology.org/project/git-sizer/versions
    pkgs_ubuntu : https://packages.ubuntu.com/git-sizer
    pkgs_debian : https://packages.debian.org/git-sizer
