    name        : git-bug
    description : Distributed, offline-first bug tracker embedded in git
    homepage    : https://github.com/MichaelMure/git-bug
    repology    : https://repology.org/project/git-bug/versions
    pkgs_vaporup: http://apt.vaporup.de/pool/main/g/git-bug/
