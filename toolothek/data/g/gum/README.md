    name        : gum
    description : a tool for glamorous shell scripts
    homepage    : https://github.com/charmbracelet/gum
    repology    : https://repology.org/project/gum-shell-tool/versions
    pkgs_vaporup: http://apt.vaporup.de/pool/main/g/gum/
