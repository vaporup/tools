    name        : gron
    description : make JSON greppable
    homepage    : https://github.com/tomnomnom/gron
    repology    : https://repology.org/project/gron/versions
    pkgs_ubuntu : https://packages.ubuntu.com/gron
    pkgs_debian : https://packages.debian.org/gron

[![Packaging status](https://repology.org/badge/vertical-allrepos/gron.svg?columns=4)](https://repology.org/project/gron/versions)
