    name        : git-crypt
    description : Transparent file encryption in git
    homepage    : https://www.agwa.name/projects/git-crypt/
    repology    : https://repology.org/project/git-crypt/versions
    pkgs_ubuntu : https://packages.ubuntu.com/git-crypt
    pkgs_debian : https://packages.debian.org/git-crypt
