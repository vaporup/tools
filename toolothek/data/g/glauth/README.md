    name        : glauth
    description : lightweight LDAP server
    homepage    : https://glauth.github.io
    repology    : https://repology.org/project/glauth/versions
    github      : https://github.com/glauth/glauth
    pkgs_vaporup: http://apt.vaporup.de/pool/main/g/glauth/
