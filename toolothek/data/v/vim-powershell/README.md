    name        : vim-powershell
    description : Syntax highlighting for Powershell files
    homepage    : https://github.com/PProvost/vim-ps1
    pkgs_vaporup: http://apt.vaporup.de/pool/main/v/vim-powershell/
