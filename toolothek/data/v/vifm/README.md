    name        : vifm
    description : file manager with Vim keyboard commands
    homepage    : https://vifm.info
    repology    : https://repology.org/project/vifm/versions
    pkgs_ubuntu : https://packages.ubuntu.com/vifm
    pkgs_debian : https://packages.debian.org/vifm
