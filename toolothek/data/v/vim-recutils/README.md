    name        : vim-recutils
    description : Syntax highlighting for GNU Recutils .rec files
    homepage    : https://github.com/zaid/vim-rec
    repology    : https://repology.org/project/vim-rec/versions
    pkgs_vaporup: http://apt.vaporup.de/pool/main/v/vim-recutils/
