    name        : vim-crystal
    description : Syntax highlighting for the Crystal language
    homepage    : https://github.com/vim-crystal/vim-crystal
    repology    : https://repology.org/project/vim-crystal/versions
    pkgs_vaporup: http://apt.vaporup.de/pool/main/v/vim-crystal/
