    name        : visidata
    description : interactive multitool for tabular data
    homepage    : https://www.visidata.org
    repology    : https://repology.org/project/visidata/versions
    pkgs_ubuntu : https://packages.ubuntu.com/visidata
    pkgs_debian : https://packages.debian.org/visidata
