    name        : vim-subtitles
    description : Syntax highlighting for subtitle files
    homepage    : https://www.linuxpages.org/projects_en.php
    repology    : https://repology.org/project/vim-subtitles/versions
    pkgs_ubuntu : https://packages.ubuntu.com/vim-subtitles
    pkgs_debian : https://packages.debian.org/vim-subtitles
