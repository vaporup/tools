    name        : virt-manager
    description : desktop application for managing virtual machines
    homepage    : https://virt-manager.org
    repology    : https://repology.org/project/virt-manager/versions
    pkgs_ubuntu : https://packages.ubuntu.com/virt-manager
    pkgs_debian : https://packages.debian.org/virt-manager
