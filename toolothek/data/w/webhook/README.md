    name        : webhook
    description : webhook server to run shell commands
    homepage    : https://github.com/adnanh/webhook
    repology    : https://repology.org/project/webhook/versions
    pkgs_ubuntu : https://packages.ubuntu.com/webhook
    pkgs_debian : https://packages.debian.org/webhook
