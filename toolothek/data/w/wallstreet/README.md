    name        : wallstreet
    description : fill your console with Wall Street-like news and stats
    homepage    : https://a.hollywood.computer
    repology    : https://repology.org/project/wallstreet/versions
    pkgs_ubuntu : https://packages.ubuntu.com/wallstreet
    pkgs_debian : https://packages.debian.org/wallstreet
