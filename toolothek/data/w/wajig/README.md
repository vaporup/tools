    name        : wajig
    description : unified package management front-end for Debian/Ubuntu
    homepage    : https://wajig.togaware.com
    repology    : https://repology.org/project/wajig/versions
    pkgs_ubuntu : https://packages.ubuntu.com/wajig
    pkgs_debian : https://packages.debian.org/wajig
