    name        : wtf
    description : the personal information dashboard for your terminal
    homepage    : https://wtfutil.com
    repology    : https://repology.org/project/wtf/versions
    pkgs_vaporup: http://apt.vaporup.de/pool/main/w/wtf/
