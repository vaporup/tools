    name        : whowatch
    description : Real-time user logins monitoring tool
    homepage    : https://github.com/mtsuszycki/whowatch/
    repology    : https://repology.org/project/whowatch/versions
    pkgs_ubuntu : https://packages.ubuntu.com/whowatch
    pkgs_debian : https://packages.debian.org/whowatch
