    name        : bat
    description : cat clone with syntax highlighting and git integration
    homepage    : https://github.com/sharkdp/bat
    pkgs_ubuntu : https://packages.ubuntu.com/bat
    pkgs_debian : https://packages.debian.org/bat
