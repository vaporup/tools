    name        : btop
    description : resource monitor that shows usage and stats
    homepage    : https://github.com/aristocratos/btop
    repology    : https://repology.org/project/btop/versions
    pkgs_ubuntu : https://packages.ubuntu.com/btop
    pkgs_debian : https://packages.debian.org/btop