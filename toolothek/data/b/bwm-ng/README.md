    name        : bwm-ng
    description : small and simple console-based bandwidth monitor
    homepage    : https://www.gropp.org/?id=projects&sub=bwm-ng
    pkgs_ubuntu : https://packages.ubuntu.com/bwm-ng
    pkgs_debian : https://packages.debian.org/bwm-ng
