    name        : bandwhich
    description : Terminal bandwidth utilization tool
    homepage    : https://github.com/imsnif/bandwhich
    pkgs_vaporup: http://apt.vaporup.de/pool/main/b/bandwhich/
