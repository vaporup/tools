    name        : bmon
    description : portable bandwidth monitor and rate estimator
    homepage    : https://github.com/tgraf/bmon/
    pkgs_ubuntu : https://packages.ubuntu.com/bmon
    pkgs_debian : https://packages.debian.org/bmon
