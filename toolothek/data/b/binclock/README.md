    name        : binclock
    description : binary clock for console with color support
    homepage    : http://www.ngolde.de/binclock.html
    pkgs_ubuntu : https://packages.ubuntu.com/binclock
    pkgs_debian : https://packages.debian.org/binclock
