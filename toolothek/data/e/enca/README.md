    name        : enca
    description : Extremely Naive Charset Analyser
    homepage    : https://cihar.com/software/enca
    pkgs_ubuntu : http://packages.ubuntu.com/enca
    pkgs_debian : http://packages.debian.org/enca
