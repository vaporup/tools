    name        : edex-ui
    description : TRON-inspired terminal emulator
    homepage    : https://github.com/GitSquared/edex-ui
    info        : https://gnulinux.ch/tron-terminal
