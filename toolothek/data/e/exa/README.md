    name        : exa
    description : modern replacement for ls
    homepage    : https://the.exa.website
    pkgs_ubuntu : https://packages.ubuntu.com/exa
    pkgs_debian : https://packages.debian.org/exa
    pkgs_vaporup: http://apt.vaporup.de/pool/main/e/exa/
