    name        : expect
    description : automates interactive applications
    homepage    : https://core.tcl-lang.org/expect/
    pkgs_ubuntu : https://packages.ubuntu.com/expect
    pkgs_debian : https://packages.debian.org/expect
