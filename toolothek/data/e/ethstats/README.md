    name        : ethstats
    description : script that quickly measures network device throughput
    homepage    : https://devel.ringlet.net/net/ethstats/
    pkgs_ubuntu : https://packages.ubuntu.com/ethstats
    pkgs_debian : https://packages.debian.org/ethstats
