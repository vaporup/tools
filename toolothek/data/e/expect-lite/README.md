    name        : expect-lite
    description : quick and easy command line automation tool
    homepage    : http://expect-lite.sourceforge.net
    pkgs_ubuntu : https://packages.ubuntu.com/expect-lite
    pkgs_debian : https://packages.debian.org/expect-lite
