    name        : ethtool
    description : display or change Ethernet device settings
    homepage    : https://mirrors.edge.kernel.org/pub/software/network/ethtool/
    pkgs_ubuntu : https://packages.ubuntu.com/ethtool
    pkgs_debian : https://packages.debian.org/ethtool
