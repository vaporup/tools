    name        : entr
    description : Run arbitrary commands when files change
    homepage    : https://eradman.com/entrproject/
    pkgs_ubuntu : https://packages.ubuntu.com/entr
    pkgs_debian : https://packages.debian.org/entr
    video       : https://www.youtube.com/watch?v=pNYRpI8lFVk
    video       : https://www.youtube.com/watch?v=9KAp_zWeI34
