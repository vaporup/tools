    name        : iftop
    description : displays bandwidth usage information on an network interface
    homepage    : https://packages.ubuntu.com/iftop
