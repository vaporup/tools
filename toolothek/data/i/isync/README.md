    name        : isync
    description : IMAP and MailDir mailbox synchronizer
    homepage    : https://isync.sourceforge.io
    repology    : https://repology.org/project/isync/versions
    pkgs_ubuntu : https://packages.ubuntu.com/isync
    pkgs_debian : https://packages.debian.org/isync
