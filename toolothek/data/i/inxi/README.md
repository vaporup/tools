    name        : inxi
    description : full featured system information script
    homepage    : https://smxi.org/docs/inxi.htm
    pkgs_ubuntu : https://packages.ubuntu.com/inxi
    pkgs_debian : https://packages.debian.org/inxi
    video       : https://www.youtube.com/watch?v=0SsjhI8GDsE
