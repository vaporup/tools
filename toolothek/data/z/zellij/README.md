    name        : zellij
    description : a terminal workspace with batteries included
    homepage    : https://zellij.dev
    repology    : https://repology.org/project/zellij/versions
    info        : https://www.linuxuprising.com/2021/04/zellij-is-new-terminal-multiplexer.html
    tag         : terminal_multiplexer
