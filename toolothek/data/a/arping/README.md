    name        : arping
    description : sends IP and/or ARP pings (to the MAC address)
    homepage    : https://www.habets.pp.se/synscan/programs_arping.html
    github      : https://github.com/ThomasHabets/arping
    pkgs_ubuntu : https://packages.ubuntu.com/arping
    pkgs_debian : https://packages.debian.org/arping
