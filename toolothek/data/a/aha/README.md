    name        : aha
    description : ANSI color to HTML converter
    homepage    : https://github.com/theZiz/aha
    pkgs_ubuntu : https://packages.ubuntu.com/aha
    pkgs_debian : https://packages.debian.org/aha
