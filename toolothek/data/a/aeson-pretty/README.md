    name        : aeson-pretty
    description : JSON pretty-printing tool
    homepage    : https://github.com/informatikr/aeson-pretty
    pkgs_ubuntu : https://packages.ubuntu.com/aeson-pretty
    pkgs_debian : https://packages.debian.org/aeson-pretty
