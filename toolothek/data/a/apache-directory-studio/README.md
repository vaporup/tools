    name        : apache-directory-studio
    description : directory tooling intended to be used with any LDAP server
    homepage    : https://directory.apache.org/studio/
    pkgs_vaporup: http://apt.vaporup.de/pool/main/a/apache-directory-studio/
