    name        : autorevision
    description : extracts revision metadata from your VCS repository
    homepage    : https://autorevision.github.io
    github      : https://github.com/Autorevision/autorevision
    pkgs_ubuntu : https://packages.ubuntu.com/autorevision
    pkgs_debian : https://packages.debian.org/autorevision
