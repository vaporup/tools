    name        : ack-grep
    description : like grep, optimized for searching
    homepage    : https://beyondgrep.com
    pkgs_ubuntu : https://packages.ubuntu.com/ack-grep
    pkgs_debian : https://packages.debian.org/ack-grep
