    name        : age
    description : the modern alternative to GPG
    homepage    : https://github.com/FiloSottile/age
    pkgs_ubuntu : https://packages.ubuntu.com/age
    pkgs_debian : https://packages.debian.org/age
    pkgs_vaporup: http://apt.vaporup.de/pool/main/a/age/
    info        : https://nixfaq.org/2021/01/age-the-modern-alternative-to-gpg.html
