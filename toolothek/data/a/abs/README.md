    name        : abs
    description : Bring back the joy of shell scripting
    homepage    : https://www.abs-lang.org
    tag         : lang
