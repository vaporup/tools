    name        : asciio
    description : create ASCII charts and graphs
    homepage    : https://metacpan.org/dist/App-Asciio
    pkgs_ubuntu : https://packages.ubuntu.com/asciio
    pkgs_debian : https://packages.debian.org/asciio
