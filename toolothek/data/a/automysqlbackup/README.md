    name        : automysqlbackup
    description : backup for your MySQL database
    homepage    : https://sourceforge.net/projects/automysqlbackup/
    pkgs_ubuntu : https://packages.ubuntu.com/automysqlbackup
    pkgs_debian : https://packages.debian.org/automysqlbackup
