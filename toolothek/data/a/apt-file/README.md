    name        : apt-file
    description : search for files within Debian packages
    homepage    : https://salsa.debian.org/apt-team/apt-file
    pkgs_ubuntu : https://packages.ubuntu.com/apt-file
    pkgs_debian : https://packages.debian.org/apt-file
