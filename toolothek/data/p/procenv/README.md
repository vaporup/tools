    name        : procenv
    description : show process environment
    homepage    : https://github.com/jamesodhunt/procenv
    repology    : https://repology.org/project/procenv/versions
    pkgs_ubuntu : https://packages.ubuntu.com/procenv
    pkgs_debian : https://packages.debian.org/procenv
