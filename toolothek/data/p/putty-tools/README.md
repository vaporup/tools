    name        : putty-tools
    description : command-line tools for SSH, SCP, and SFTP
    homepage    : https://www.chiark.greenend.org.uk/~sgtatham/putty/
    repology    : https://repology.org/project/putty/versions
    pkgs_ubuntu : https://packages.ubuntu.com/putty-tools
    pkgs_debian : https://packages.debian.org/putty-tools
