    name        : pwgen
    description : generate random password
    homepage    : https://github.com/tytso/pwgen
    repology    : https://repology.org/project/pwgen/versions
    pkgs_ubuntu : https://packages.ubuntu.com/pwgen
    pkgs_debian : https://packages.debian.org/pwgen
    info        : https://wiki.ubuntuusers.de/pwgen
