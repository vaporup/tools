    name        : pinta
    description : Simple drawing/painting program
    homepage    : https://www.pinta-project.com
    repology    : https://repology.org/project/pinta/versions
    pkgs_ubuntu : https://packages.ubuntu.com/pinta
    pkgs_debian : https://packages.debian.org/pinta
