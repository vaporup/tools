    name        : pv
    description : pipeline to meter data passing through
    homepage    : https://www.ivarch.com/programs/pv.shtml
    repology    : https://repology.org/project/pv/versions
    pkgs_ubuntu : https://packages.ubuntu.com/pv
    pkgs_debian : https://packages.debian.org/pv

# Example

```
pv mysql-db.sql | mysql
```
