    name        : pandoc
    description : general markup converter
    homepage    : https://pandoc.org
    repology    : https://repology.org/project/pandoc/versions
    pkgs_ubuntu : https://packages.ubuntu.com/pandoc
    pkgs_debian : https://packages.debian.org/pandoc
