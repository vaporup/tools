    name        : pexpect
    description : Python module for automating interactive application
    homepage    : https://github.com/pexpect/pexpect
    repology    : https://repology.org/project/pexpect/versions
    pkgs_ubuntu : https://packages.ubuntu.com/python3-pexpect
    pkgs_deboam : https://packages.debian.org/python3-pexpect
