    name        : pyp
    description : sed/awk-like tool with Python language
    homepage    : https://github.com/hauntsaninja/pyp
    repology    : https://repology.org/project/pyp/versions
    pkgs_ubuntu : https://packages.ubuntu.com/pyp
    pkgs_debian : https://packages.debian.org/pyp
    video       : https://www.youtube.com/watch?v=eWtVWF0JSJA
    video       : https://www.youtube.com/watch?v=3UHE-zD1r_M
