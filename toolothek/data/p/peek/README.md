    name        : peek
    description : simple screen recorder
    homepage    : https://github.com/phw/peek
    repology    : https://repology.org/project/peek/versions
    pkgs_ubuntu : https://packages.ubuntu.com/peek
    pkgs_debian : https://packages.debian.org/peek
    video       : https://www.youtube.com/watch?v=XgiZlQqaWu8
