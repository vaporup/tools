    name        : pktstat
    description : top-like utility for network connections usage
    homepage    : https://github.com/dleonard0/pktstat
    repology    : https://repology.org/project/pktstat/versions
    pkgs_ubuntu : https://packages.ubuntu.com/pktstat
    pkgs_debian : https://packages.debian.org/pktstat
