    name        : ping-tools
    description : ping a specific TCP port
    homepage    : https://github.com/vaporup/ping-tools
    pkgs_vaporup: http://apt.vaporup.de/pool/main/p/ping-tools/
