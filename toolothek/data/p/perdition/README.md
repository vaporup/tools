    name        : perdition
    description : POP3, IMAP4 and managesieve proxy server
    homepage    : https://projects.horms.net/projects/perdition/
    repology    : https://repology.org/project/perdition/versions
    pkgs_ubuntu : https://packages.ubuntu.com/perdition
    pkgs_debian : https://packages.debian.org/perdition
