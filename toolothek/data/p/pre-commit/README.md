    name        : pre-commit
    description : Git pre-commit hook framework
    homepage    : https://pre-commit.com
    repology    : https://repology.org/project/python:pre-commit/versions
    pkgs_ubuntu : https://packages.ubuntu.com/pre-commit
    pkgs_debian : https://packages.debian.org/pre-commit
    apropos     : https://github.com/scop/pre-commit-shfmt
    tag         : hook
    tag         : git