    name        : putty
    description : SSH client for X
    homepage    : https://www.chiark.greenend.org.uk/~sgtatham/putty/
    repology    : https://repology.org/project/putty/versions
    pkgs_ubuntu : https://packages.ubuntu.com/putty
    pkgs_debian : https://packages.debian.org/putty
