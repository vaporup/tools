    name        : pydf
    description : colourised df-clone
    homepage    : http://kassiopeia.juls.savba.sk/~garabik/software/pydf/
    repology    : https://repology.org/project/pydf/versions
    pkgs_ubuntu : https://packages.ubuntu.com/pydf
    pkgs_debian : https://packages.debian.org/pydf
