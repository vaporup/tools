    name        : pykwalify
    description : YAML/JSON schema validation
    homepage    : https://github.com/grokzen/pykwalify
    repology    : https://repology.org/project/pykwalify/versions
    pkgs_ubuntu : https://packages.ubuntu.com/pykwalify
    pkgs_debian : https://packages.debian.org/pykwalify
    info        : https://github.com/Grokzen/pykwalify/tree/master/tests
