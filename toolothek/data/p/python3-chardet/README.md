    name        : python3-chardet
    description : universal character encoding detector
    homepage    : https://github.com/chardet/chardet
    repology    : https://repology.org/project/chardet/versions
    pkgs_ubuntu : http://packages.ubuntu.com/python3-chardet
    pkgs_debian : http://packages.debian.org/python3-chardet
