    name        : sqlline
    description : JDBC command-line utility for issuing SQL
    homepage    : https://sqlline.sourceforge.net/
    repology    : https://repology.org/project/sqlline/versions
    pkgs_ubuntu : https://packages.ubuntu.com/sqlline
    pkgs_debian : https://packages.debian.org/sqlline
    fork        : https://github.com/julianhyde/sqlline
