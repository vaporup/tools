    name        : ssdeep
    description : recursive piecewise hashing tool
    homepage    : https://ssdeep-project.github.io/ssdeep/
    repology    : https://repology.org/project/ssdeep/versions
    pkgs_ubuntu : https://packages.ubuntu.com/ssdeep
    pkgs_debian : https://packages.debian.org/ssdeep
