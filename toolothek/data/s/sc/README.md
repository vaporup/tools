    name        : sc
    description : text-based spreadsheet calculator
    repology    : https://repology.org/project/sc/versions
    pkgs_ubuntu : https://packages.ubuntu.com/sc
    pkgs_debian : https://packages.debian.org/sc
    apropos     : sc-im
    info        : https://www.baeldung.com/linux/command-line-edit-spreadsheets
    info        : https://www.xmodulo.com/make-spreadsheets-linux-terminal.html
    info        : https://www.maketecheasier.com/linux-command-line-spreadsheets
