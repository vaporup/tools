    name        : sftpgo
    description : SSH/SFTP and HTTPS/WebDAV Server
    homepage    : https://github.com/drakkan/sftpgo
    repology    : https://repology.org/project/sftpgo/versions
    tag         : sftp
    tag         : ssh
    tag         : webdav
    tag         : s3
