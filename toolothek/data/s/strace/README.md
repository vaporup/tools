    name        : strace
    description : A system call tracer
    homepage    : https://strace.io
    repology    : https://repology.org/project/strace/versions
    pkgs_ubuntu : https://packages.ubuntu.com/strace
    pkgs_debian : https://packages.debian.org/strace
