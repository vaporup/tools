    name        : ssh-askpass
    description : under X, asks user for a passphrase for ssh-add
    homepage    : https://github.com/theseal/ssh-askpass
    repology    : https://repology.org/project/ssh-askpass/versions
    pkgs_ubuntu : https://packages.ubuntu.com/ssh-askpass
    pkgs_debian : https://packages.debian.org/ssh-askpass
