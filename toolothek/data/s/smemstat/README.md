    name        : smemstat
    description : memory usage monitoring tool
    homepage    : https://github.com/ColinIanKing/smemstat
    repology    : https://repology.org/project/smemstat/versions
    pkgs_ubuntu : https://packages.ubuntu.com/smemstat
    pkgs_debian : https://packages.debian.org/smemstat