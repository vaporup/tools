    name        : sshpass
    description : Non-interactive ssh password authentication
    homepage    : https://sourceforge.net/projects/sshpass/
    repology    : https://repology.org/project/sshpass/versions
    pkgs_ubuntu : https://packages.ubuntu.com/sshpass
    pkgs_debian : https://packages.debian.org/sshpass
