    name        : shfmt
    description : format shell programs
    homepage    : https://github.com/mvdan/sh
    repology    : https://repology.org/project/shfmt/versions
    pkgs_ubuntu : https://packages.ubuntu.com/shfmt
    pkgs_debian : https://packages.debian.org/shfmt
    apropos     : https://github.com/scop/pre-commit-shfmt