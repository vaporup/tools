    name        : shellcheck
    description : lint tool for shell scripts
    homepage    : https://www.shellcheck.net
    repology    : https://repology.org/project/shellcheck/versions
    pkgs_ubuntu : https://packages.ubuntu.com/shellcheck
    pkgs_debian : https://packages.debian.org/shellcheck
    tag         : lint
