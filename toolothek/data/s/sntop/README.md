    name        : sntop
    description : curses-based utility that polls hosts for connectivity
    homepage    : https://sourceforge.net/projects/sntop/
    repology    : https://repology.org/project/sntop/versions
    pkgs_ubuntu : https://packages.ubuntu.com/sntop
    pkgs_debian : https://packages.debian.org/sntop
