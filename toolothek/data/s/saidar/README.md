    name        : saidar
    description : curses-based program which displays live system statistics
    homepage    : https://libstatgrab.org
    repology    : https://repology.org/project/libstatgrab/versions
    pkgs_ubuntu : https://packages.ubuntu.com/saidar
    pkgs_debian : https://packages.debian.org/saidar
