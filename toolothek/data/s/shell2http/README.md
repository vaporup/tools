    name        : shell2http
    description : HTTP-server to execute shell commands
    homepage    : https://github.com/msoap/shell2http
    repology    : https://repology.org/project/shell2http/versions
    pkgs_vaporup: http://apt.vaporup.de/pool/main/s/shell2http/
