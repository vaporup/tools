    name        : ssh-tools
    description : collection of various tools using ssh
    homepage    : https://codeberg.org/vaporup/ssh-tools
    repology    : https://repology.org/project/ssh-tools/versions
    pkgs_ubuntu : https://packages.ubuntu.com/ssh-tools
    pkgs_debian : https://packages.debian.org/ssh-tools
