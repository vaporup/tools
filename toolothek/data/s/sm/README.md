    name        : sm
    description : displays a short text fullscreen
    homepage    : https://www.joachim-breitner.de/projects#screen-message
    pkgs_ubuntu : https://packages.ubuntu.com/sm
    pkgs_debian : https://packages.debian.org/sm
    tag         : notify