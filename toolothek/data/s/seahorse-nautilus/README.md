    name        : seahorse-nautilus
    description : Nautilus extension for Seahorse integration
    homepage    : https://wiki.gnome.org/Apps/Seahorse
    repology    : https://repology.org/project/seahorse-nautilus/versions
    pkgs_ubuntu : https://packages.ubuntu.com/seahorse-nautilus
    pkgs_debian : https://packages.debian.org/seahorse-nautilus
