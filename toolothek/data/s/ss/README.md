    name        : ss
    description : Display Linux TCP / UDP Network and Socket Information
    homepage    : https://wiki.linuxfoundation.org/networking/iproute2
    repology    : https://repology.org/project/iproute2/versions
    pkgs_ubuntu : https://packages.ubuntu.com/iproute2
    pkgs_debian : https://packages.debian.org/iproute2
