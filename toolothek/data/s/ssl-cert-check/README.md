    name        : ssl-cert-check
    description : proactively handling X.509 certificate expiration
    homepage    : https://prefetch.net/articles/checkcertificate.html
    repology    : https://repology.org/project/ssl-cert-check/versions
    pkgs_ubuntu : https://packages.ubuntu.com/ssl-cert-check
    pkgs_debian : https://packages.debian.org/ssl-cert-check
    tag         : monitoring
