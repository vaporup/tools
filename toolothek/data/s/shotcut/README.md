    name        : shotcut
    description : video editor
    homepage    : https://www.shotcut.org
    repology    : https://repology.org/project/shotcut/versions
    pkgs_ubuntu : https://packages.ubuntu.com/shotcut
    pkgs_debian : https://packages.debian.org/shotcut
