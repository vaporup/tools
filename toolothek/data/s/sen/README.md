    name        : sen
    description : Terminal user interface for docker engine
    homepage    : https://github.com/TomasTomecek/sen
    repology    : https://repology.org/project/sen/versions
    pkgs_ubuntu : https://packages.ubuntu.com/sen
    pkgs_debian : https://packages.debian.org/sen
