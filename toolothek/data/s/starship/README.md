    name        : starship
    description : cross-shell prompt
    homepage    : https://starship.rs
    repology    : https://repology.org/project/starship/versions
    pkgs_vaporup: http://apt.vaporup.de/pool/main/s/starship/
