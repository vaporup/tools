    name        : smem
    description : memory reporting tool
    homepage    : https://www.selenic.com/smem/
    repology    : https://repology.org/project/smem/versions
    pkgs_ubuntu : https://packages.ubuntu.com/smem
    pkgs_debian : https://packages.debian.org/smem