    name        : stunnel
    description : add TLS encryption to clients and servers
    homepage    : https://www.stunnel.org
    repology    : https://repology.org/project/stunnel/versions
    pkgs_ubuntu : https://packages.ubuntu.com/stunnel
    pkgs_debian : https://packages.debian.org/stunnel
