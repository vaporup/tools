    name        : silversearcher-ag
    description : very fast grep-like program, alternative to ack-grep
    homepage    : https://geoff.greer.fm/ag/
    repology    : https://repology.org/project/the-silver-searcher/versions
    github      : https://github.com/ggreer/the_silver_searcher
    pkgs_ubuntu : https://packages.ubuntu.com/silversearcher-ag
    pkgs_debian : https://packages.debian.org/silversearcher-ag
