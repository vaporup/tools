    name        : sysstat
    description : system performance tools for Linux
    homepage    : http://sebastien.godard.pagesperso-orange.fr
    repology    : https://repology.org/project/sysstat/versions
    pkgs_ubuntu : https://packages.ubuntu.com/sysstat
    pkgs_debian : https://packages.debian.org/sysstat
