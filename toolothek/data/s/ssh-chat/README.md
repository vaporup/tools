    name        : ssh-chat
    description : chat over SSH
    homepage    : https://github.com/shazow/ssh-chat
    repology    : https://repology.org/project/ssh-chat/versions
    pkgs_vaporup: http://apt.vaporup.de/pool/main/s/ssh-chat/
    tag         : chat
    tag         : ssh