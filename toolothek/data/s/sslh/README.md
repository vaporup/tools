    name        : sslh
    description : protocol multiplexer
    homepage    : https://github.com/yrutschle/sslh
    repology    : https://repology.org/project/sslh/versions
    pkgs_ubuntu : https://packages.ubuntu.com/sslh
    pkgs_debian : https://packages.debian.org/sslh
    info        : https://ostechnix.com/sslh-share-port-https-ssh/
    info        : https://www.unixmen.com/sslh-a-sslssh-multiplexer-for-linux/

[![Packaging status](https://repology.org/badge/vertical-allrepos/sslh.svg?columns=4)](https://repology.org/project/sslh/versions)
