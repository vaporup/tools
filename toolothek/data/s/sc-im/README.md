    name        : sc-im
    description : text-based spreadsheet calculator
    homepage    : https://github.com/andmarti1424/sc-im
    repology    : https://repology.org/project/sc-im/versions
    pkgs_ubuntu : https://packages.ubuntu.com/sc-im
    pkgs_debian : https://packages.debian.org/sc-im
    apropos     : sc
    info        : https://www.baeldung.com/linux/command-line-edit-spreadsheets
    info        : https://www.xmodulo.com/make-spreadsheets-linux-terminal.html
    info        : https://www.maketecheasier.com/linux-command-line-spreadsheets
    video       : https://www.youtube.com/watch?v=K_8_gazN7h0