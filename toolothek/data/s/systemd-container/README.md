    name        : systemd-container
    description : systemd container/nspawn tools
    pkgs_ubuntu : https://packages.ubuntu.com/systemd-container
    pkgs_debian : https://packages.debian.org/systemd-container
