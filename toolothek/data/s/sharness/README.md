    name        : sharness
    description : shell library for automated tests with TAP output
    homepage    : http://chriscool.github.io/sharness/
    repology    : https://repology.org/project/sharness/versions
    pkgs_ubuntu : https://packages.ubuntu.com/sharness
    pkgs_debian : https://packages.debian.org/sharness
    tag         : test
