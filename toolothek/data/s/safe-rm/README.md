    name        : safe-rm
    description : wrapper around the rm command to prevent accidental deletions
    homepage    : https://launchpad.net/safe-rm
    repology    : https://repology.org/project/safe-rm/versions
    pkgs_ubuntu : https://packages.ubuntu.com/safe-rm
    pkgs_debian : https://packages.debian.org/safe-rm
