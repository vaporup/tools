    name        : sysnews
    description : display system news
    homepage    : https://www.ibiblio.org/pub/Linux/system/admin/login/
    repology    : https://repology.org/project/sysnews/versions
    pkgs_ubuntu : https://packages.ubuntu.com/sysnews
    pkgs_debian : https://packages.debian.org/sysnews
