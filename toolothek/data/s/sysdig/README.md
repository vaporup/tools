    name        : sysdig
    description : capture system state and activity then save, filter and analyze
    homepage    : https://sysdig.com
    repology    : https://repology.org/project/sysdig/versions
    pkgs_ubuntu : https://packages.ubuntu.com/sysdig
    pkgs_debian : https://packages.debian.org/sysdig
