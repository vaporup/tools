    name        : subtitles
    description : handle video subtitles in various text formats
    homepage    : https://metacpan.org/pod/Subtitles
    repology    : https://repology.org/project/perl:subtitles/versions
    pkgs_ubuntu : https://packages.ubuntu.com/libsubtitles-perl
    pkgs_debian : https://packages.debian.org/libsubtitles-perl
