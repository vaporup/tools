    name        : usbip
    description : sharing USB devices over the network
    pkgs_debian : https://packages.debian.org/usbip
    pkgs_ubuntu : https://packages.ubuntu.com/usbip
