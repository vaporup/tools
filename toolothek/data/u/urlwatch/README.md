    name        : urlwatch
    description : monitoring webpages for updates
    homepage    : https://thp.io/2008/urlwatch/
    repology    : https://repology.org/project/urlwatch/versions
    pkgs_ubuntu : https://packages.ubuntu.com/urlwatch
    pkgs_debian : https://packages.debian.org/urlwatch
