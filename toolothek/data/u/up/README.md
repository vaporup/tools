    name        : up
    description : writing Linux pipes with instant live preview
    homepage    : https://github.com/akavel/up
    repology    : https://repology.org/project/up-plumber/versions
    pkgs_vaporup: http://apt.vaporup.de/pool/main/u/up/
