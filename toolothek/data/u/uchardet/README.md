    name        : uchardet
    description : universal charset detection
    homepage    : https://www.freedesktop.org/wiki/Software/uchardet/
    repology    : https://repology.org/project/uchardet/versions
    pkgs_ubuntu : http://packages.ubuntu.com/uchardet
    pkgs_debian : http://packages.debian.org/uchardet
