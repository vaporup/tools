    name        : mtr
    description : combines the functionality of the traceroute and ping
    homepage    : https://www.bitwizard.nl/mtr/
    repology    : https://repology.org/project/mtr/versions
    pkgs_ubuntu : https://packages.ubuntu.com/mtr
    pkgs_debian : https://packages.debian.org/mtr
