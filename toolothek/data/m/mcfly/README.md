    name        : mcfly
    description : Fly through your shell history. Great Scott!
    homepage    : https://github.com/cantino/mcfly
    repology    : https://repology.org/project/mcfly/versions
    pkgs_vaporup: http://apt.vaporup.de/pool/main/m/mcfly/
