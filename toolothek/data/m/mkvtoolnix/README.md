    name        : mkvtoolnix
    description : tools to work with Matroska files - CLI
    homepage    : https://mkvtoolnix.download
    repology    : https://repology.org/project/mkvtoolnix/versions
    pkgs_ubuntu : https://packages.ubuntu.com/mkvtoolnix
    pkgs_debian : https://packages.debian.org/mkvtoolnix
