    name        : mc
    description : Midnight Commander - a powerful file manager
    homepage    : https://midnight-commander.org
    repology    : https://repology.org/project/mc/versions
    pkgs_ubuntu : https://packages.ubuntu.com/mc
    pkgs_debian : https://packages.debian.org/mc
