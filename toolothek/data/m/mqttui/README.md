    name        : mqttui
    description : MQTT subscribe or publish from the terminal
    homepage    : https://github.com/EdJoPaTo/mqttui
    repology    : https://repology.org/project/mqttui/versions
    pkgs_vaporup: http://apt.vaporup.de/pool/main/m/mqttui/
