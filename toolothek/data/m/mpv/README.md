    name        : mpv
    description : video player based on MPlayer/mplayer2
    homepage    : https://mpv.io
    repology    : https://repology.org/project/mpv/versions
    pkgs_ubuntu : https://packages.ubuntu.com/mpv
    pkgs_debian : https://packages.debian.org/mpv
