    name        : maildirsync
    description : Maildir synchronisation
    homepage    : https://code.google.com/archive/p/maildirsync/
    repology    : https://repology.org/project/maildirsync/versions
    pkgs_ubuntu : https://packages.ubuntu.com/maildirsync
    pkgs_debian : https://packages.debian.org/maildirsync
