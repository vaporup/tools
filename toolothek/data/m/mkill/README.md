    name        : mkill
    description : M-M-M MONSTER KILL Linux processes
    homepage    : https://github.com/vaporup/mkill
    pkgs_vaporup: http://apt.vaporup.de/pool/main/m/mkill/
