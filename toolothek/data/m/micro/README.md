    name        : micro
    description : modern and intuitive terminal-based text editor
    homepage    : https://micro-editor.github.io
    repology    : https://repology.org/project/micro/versions
    pkgs_ubuntu : https://packages.ubuntu.com/micro
    pkgs_debian : https://packages.debian.org/micro
