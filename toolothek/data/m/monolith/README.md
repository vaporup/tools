    name        : monolith
    description : CLI tool for saving complete web pages as a single HTML file
    homepage    : https://github.com/Y2Z/monolith
    repology    : https://repology.org/project/monolith/versions
    pkgs_vaporup: http://apt.vaporup.de/pool/main/m/monolith/
