    name        : monit
    description : Easy, proactive monitoring
    homepage    : https://mmonit.com/monit/
    repology    : https://repology.org/project/monit/versions
    pkgs_ubuntu : https://packages.ubuntu.com/monit
    pkgs_debian : https://packages.debian.org/monit
    tag         : monitoring
