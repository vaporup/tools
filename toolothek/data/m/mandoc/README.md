    name        : mandoc
    description : manpage compiler toolset
    homepage    : https://mandoc.bsd.lv
    repology    : https://repology.org/project/mandoc/versions
    pkgs_ubuntu : https://packages.ubuntu.com/mandoc
    pkgs_debian : https://packages.debian.org/mandoc
