    name        : multitail
    description : view multiple logfiles windowed on console
    homepage    : https://www.vanheusden.com/multitail/
    repology    : https://repology.org/project/multitail/versions
    pkgs_ubuntu : https://packages.ubuntu.com/multitail
    pkgs_debian : https://packages.debian.org/multitail
