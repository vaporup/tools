    name        : mdbook
    description : create books from markdown files like Gitbook
    homepage    : https://github.com/rust-lang/mdBook
    repology    : https://repology.org/project/mdbook/versions
    pkgs_vaporup: http://apt.vaporup.de/pool/main/m/mdbook/
