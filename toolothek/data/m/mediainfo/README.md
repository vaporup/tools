    name        : mediainfo
    description : reading information from audio/video files - CLI
    homepage    : https://mediaarea.net/en/MediaInfo
    repology    : https://repology.org/project/mediainfo/versions
    pkgs_ubuntu : https://packages.ubuntu.com/mediainfo
    pkgs_debian : https://packages.debian.org/mediainfo
