    name        : miller
    description : like awk, sed, cut, join, and sort for CSV, TSV, JSON
    homepage    : https://miller.readthedocs.io
    repology    : https://repology.org/project/miller/versions
    github      : https://github.com/johnkerl/miller
    pkgs_ubuntu : https://packages.ubuntu.com/miller
    pkgs_debian : https://packages.debian.org/miller
