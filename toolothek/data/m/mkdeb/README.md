    name        : mkdeb
    description : quickly create the files for a deb package
    homepage    : https://vaporup.github.io/books/the-pragmatic-sysadmin/debian-packaging/mkdeb.html
    pkgs_vaporup: http://apt.vaporup.de/pool/main/m/mkdeb/
