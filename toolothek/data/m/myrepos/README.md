    name        : myrepos
    description : manage all your version control repos
    homepage    : https://myrepos.branchable.com
    repology    : https://repology.org/project/myrepos/versions
    pkgs_ubuntu : https://packages.ubuntu.com/myrepos
    pkgs_debian : https://packages.debian.org/myrepos
