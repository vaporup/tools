    name        : mdless
    description : view of Markdown files in Terminal
    homepage    : https://github.com/ttscoff/mdless
    pkgs_vaporup: http://apt.vaporup.de/pool/main/m/mdless/
