    name        : molly-guard
    description : protects machines from accidental shutdowns/reboots
    repology    : https://repology.org/project/molly-guard/versions
    pkgs_ubuntu : https://packages.ubuntu.com/molly-guard
    pkgs_debian : https://packages.debian.org/molly-guard
