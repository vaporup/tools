    name        : mindustry
    description : automation tower defense game
    homepage    : https://mindustrygame.github.io
    repology    : https://repology.org/project/mindustry/versions
    pkgs_vaporup: http://apt.vaporup.de/pool/main/m/mindustry/
    tag         : game
