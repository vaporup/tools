    name        : mssh
    description : administrate multiple servers at once
    homepage    : https://hgarcia.es
    repology    : https://repology.org/project/mssh/versions
    pkgs_ubuntu : https://packages.ubuntu.com/mssh
    pkgs_debian : https://packages.debian.org/mssh

[![Packaging status](https://repology.org/badge/vertical-allrepos/mssh.svg?columns=4)](https://repology.org/project/mssh/versions)
