    name        : meld
    description : graphical tool to diff and merge files
    homepage    : http://meldmerge.org
    repology    : https://repology.org/project/meld/versions
    pkgs_ubuntu : https://packages.ubuntu.com/meld
    pkgs_debian : https://packages.debian.org/meld
    tag         : diff
