    name        : mtree-netbsd
    description : Validates directory tree against specification
    homepage    : http://cdn.netbsd.org/pub/pkgsrc/current/pkgsrc/pkgtools/mtree/README.html
    repology    : https://repology.org/project/mtree-netbsd/versions
    pkgs_ubuntu : https://packages.ubuntu.com/mtree-netbsd
    pkgs_debian : https://packages.debian.org/mtree-netbsd
