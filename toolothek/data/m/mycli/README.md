    name        : mycli
    description : CLI for MySQL with auto-completion and syntax highlighting
    homepage    : https://www.mycli.net
    repology    : https://repology.org/project/mycli/versions
    pkgs_ubuntu : https://packages.ubuntu.com/mycli
    pkgs_debian : https://packages.debian.org/mycli
