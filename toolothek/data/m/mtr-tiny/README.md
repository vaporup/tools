    name        : mtr-tiny
    description : ncurses traceroute tool
    homepage    : https://www.bitwizard.nl/mtr/
    repology    : https://repology.org/project/mtr/versions
    pkgs_ubuntu : https://packages.ubuntu.com/mtr-tiny
    pkgs_debian : https://packages.debian.org/mtr-tiny
