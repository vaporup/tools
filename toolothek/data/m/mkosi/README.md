    name        : mkosi
    description : create legacy-free OS images
    homepage    : https://github.com/systemd/mkosi
    repology    : https://repology.org/project/mkosi/versions
    pkgs_ubuntu : https://packages.ubuntu.com/mkosi
    pkgs_debian : https://packages.debian.org/mkosi
