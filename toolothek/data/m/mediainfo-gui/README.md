    name        : mediainfo-gui
    description : reading information from audio/video files - GUI
    homepage    : https://mediaarea.net/en/MediaInfo
    repology    : https://repology.org/project/mediainfo-gui/versions
    pkgs_ubuntu : https://packages.ubuntu.com/mediainfo-gui
    pkgs_debian : https://packages.debian.org/mediainfo-gui
