    name        : yajl-tools
    description : Yet Another JSON Library - tools
    repology    : https://repology.org/project/yajl/versions
    pkgs_ubuntu : https://packages.ubuntu.com/yajl-tools
    pkgs_debian : https://packages.debian.org/yajl-tools
