    name        : yq
    description : portable command-line YAML processor
    homepage    : https://github.com/mikefarah/yq
    repology    : https://repology.org/project/yq-mikefarah/versions
    pkgs_vaporup: http://apt.vaporup.de/pool/main/y/yq/
