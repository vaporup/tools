    name        : yamllint
    description : Linter for YAML files
    homepage    : https://github.com/adrienverge/yamllint
    repology    : https://repology.org/project/yamllinr/versions
    pkgs_debian: https://packages.debian.org/yamllint
    pkgs_ubuntu: https://packages.ubuntu.com/yamllint
