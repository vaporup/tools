    name        : yj
    description : Convert between YAML, TOML, JSON, and HCL
    homepage    : https://github.com/sclevine/yj
    repology    : https://repology.org/project/yj-sclevine/versions
    pkgs_vaporup: http://apt.vaporup.de/pool/main/y/yj/
