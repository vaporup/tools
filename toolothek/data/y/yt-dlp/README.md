    name        : yt-dlp
    description : youtube-dl fork with additional features and fixes
    homepage    : https://github.com/yt-dlp/yt-dlp
    repology    : https://repology.org/project/yt-dlp/versions
    pkgs_ubuntu : https://packages.ubuntu.com/yt-dlp
    pkgs_debian : https://packages.debian.org/yt-dlp
    pkgs_vaporup: http://apt.vaporup.de/pool/main/y/yt-dlp/
