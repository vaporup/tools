    name        : xz-utils
    description : XZ-format compression utilities
    homepage    : https://tukaani.org/xz/
    repology    : https://repology.org/project/xz/versions
    pkgs_ubuntu : https://packages.ubuntu.com/xz-utils
    pkgs_debian : https://packages.debian.org/xz-utils
