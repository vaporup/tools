    name        : xxd
    description : make (or reverse) a hex dump
    pkgs_ubuntu : https://packages.ubuntu.com/xxd
    pkgs_debian : https://packages.debian.org/xxd
