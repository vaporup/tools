    name        : xh
    description : Friendly and fast tool for sending HTTP requests
    homepage    : https://github.com/ducaale/xh
    repology    : https://repology.org/project/xh/versions
    pkgs_vaporup: http://apt.vaporup.de/pool/main/x/xh/
