    name        : xonsh
    description : Unix-gazing shell
    homepage    : https://xon.sh
    repology    : https://repology.org/project/xonsh/versions
    pkgs_ubuntu : https://packages.ubuntu.com/xonsh
    pkgs_debian : https://packages.debian.org/xonsh
    info        : https://opensource.com/article/18/9/xonsh-bash-alternative
