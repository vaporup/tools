    name        : filters
    description : text filters to munge written language in amusing ways
    homepage    : http://joeyh.name/code/filters/
    repology    : https://repology.org/project/filters/versions
    pkgs_ubuntu : https://packages.ubuntu.com/filters
    pkgs_debian : https://packages.debian.org/filters
    video       : https://www.youtube.com/watch?v=AnokespTYz4
    tag         : fun
