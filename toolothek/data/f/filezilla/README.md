    name        : filezilla
    description : graphical FTP/FTPS/SFTP client
    homepage    : https://filezilla-project.org
    repology    : https://repology.org/project/filezilla/versions
    pkgs_ubuntu : https://packages.ubuntu.com/filezilla
    pkgs_debian : https://packages.debian.org/filezilla
