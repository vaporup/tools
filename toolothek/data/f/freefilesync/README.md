    name        : freefilesync
    description : cross-platform file sync utility
    homepage    : https://freefilesync.org
    repology    : https://repology.org/project/freefilesync/versions
    pkgs_ubuntu : https://packages.ubuntu.com/freefilesync
    pkgs_debian : https://packages.debian.org/freefilesync
