    name        : flameshot
    description : screenshot software
    homepage    : https://flameshot.org
    repology    : https://repology.org/project/flameshot/versions
    pkgs_ubuntu : https://packages.ubuntu.com/flameshot
    pkgs_debian : https://packages.debian.org/flameshot
