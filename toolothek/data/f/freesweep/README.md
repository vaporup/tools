    name        : freesweep
    description : text-based minesweeper
    homepage    : https://github.com/rwestlund/freesweep
    repology    : https://repology.org/project/freesweep/versions
    pkgs_ubuntu : https://packages.ubuntu.com/freesweep
    pkgs_debian : https://packages.debian.org/freesweep
    tag         : game
