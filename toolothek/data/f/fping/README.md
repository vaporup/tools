    name        : fping
    description : sends ICMP ECHO REQUEST packets to network hosts
    homepage    : https://www.fping.org
    repology    : https://repology.org/project/fping/versions
    pkgs_ubuntu : https://packages.ubuntu.com/fping
    pkgs_debian : https://packages.debian.org/fping
