    name        : fluent-bit
    description : Fast and Lightweight Log processor and forwarder
    homepage    : https://fluentbit.io
    repology    : https://repology.org/project/fluent-bit/versions
    tag         : logging
