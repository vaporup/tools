    name        : fspy
    description : filesystem activity monitoring tool
    pkgs_ubuntu : https://packages.ubuntu.com/fspy
    pkgs_debian : https://packages.debian.org/fspy
    tag         : monitoring
