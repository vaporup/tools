    name        : freerdp2-x11
    description : RDP client for Windows Terminal Services
    homepage    : https://www.freerdp.com
    repology    : https://repology.org/project/freerdp/versions
    pkgs_ubuntu : https://packages.ubuntu.com/freerdp2-x11
    pkgs_debian : https://packages.debian.org/freerdp2-x11
