    name        : fzf
    description : command-line fuzzy finder
    homepage    : https://github.com/junegunn/fzf
    repology    : https://repology.org/project/fzf/versions
    pkgs_ubuntu : https://packages.ubuntu.com/fzf
    pkgs_debian : https://packages.debian.org/fzf
