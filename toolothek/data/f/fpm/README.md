    name        : fpm
    description : Build packages (deb, rpm, etc) with great ease and sanity
    homepage    : https://github.com/jordansissel/fpm
    repology    : https://repology.org/project/fpm-package-builder/versions
