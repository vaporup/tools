    name        : fdupes
    description : identifies duplicate files within given directories
    homepage    : https://github.com/adrianlopezroche/fdupes
    repology    : https://repology.org/project/fdupes/versions
    pkgs_ubuntu : https://packages.ubuntu.com/fdupes
    pkgs_debian : https://packages.debian.org/fdupes
