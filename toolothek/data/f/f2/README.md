    name        : f2
    description : command-line tool for batch renaming
    homepage    : https://github.com/ayoisaiah/f2
    repology    : https://repology.org/project/f2/versions
    pkgs_vaporup: http://apt.vaporup.de/pool/main/f/f2/
