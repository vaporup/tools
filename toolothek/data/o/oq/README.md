    name        : oq
    description : jq wrapper for formats other than JSON
    homepage    : https://blacksmoke16.github.io/oq/
    repology    : https://repology.org/project/oq/versions
    pkgs_vaporup: http://apt.vaporup.de/pool/main/o/oq/
