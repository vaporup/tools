    name        : onefetch
    description : Git repository summary on your terminal
    homepage    : https://github.com/o2sh/onefetch
    repology    : https://repology.org/project/onefetch/versions
    pkgs_vaporup: http://apt.vaporup.de/pool/main/o/onefetch/
