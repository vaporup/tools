    name        : oh-my-git
    description : game about learning Git
    homepage    : https://ohmygit.org
    repology    : https://repology.org/project/oh-my-git/versions
    pkgs_vaporup: http://apt.vaporup.de/pool/main/o/oh-my-git/
    tag         : game
