    name        : ondir
    description : automate tasks specific to certain directories
    homepage    : https://swapoff.org/ondir.html
    repology    : https://repology.org/project/ondir/versions
    pkgs_debian : https://packages.debian.org/ondir
    pkgs_ubuntu : https://packages.ubuntu.com/ondir
