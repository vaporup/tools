    name        : nload
    description : realtime console network usage monitor
    homepage    : http://www.roland-riegel.de/nload/
    repology    : https://repology.org/project/nload/versions
    pkgs_ubuntu : https://packages.ubuntu.com/nload
    pkgs_ubuntu : https://packages.debian.org/nload
