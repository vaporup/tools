    name        : nicstat
    description : network traffic statistics
    homepage    : https://github.com/scotte/nicstat/
    repology    : https://repology.org/project/nicstat/versions
    pkgs_ubuntu : https://packages.ubuntu.com/nicstat
    pkgs_debian : https://packages.debian.org/nicstat
