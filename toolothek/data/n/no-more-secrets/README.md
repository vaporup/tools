    name        : no-more-secrets
    description : data decryption effect seen in the 1992 movie Sneakers
    homepage    : https://github.com/bartobri/no-more-secrets
    repology    : https://repology.org/project/no-more-secrets/versions
    pkgs_vaporup: http://apt.vaporup.de/pool/main/n/no-more-secrets/
