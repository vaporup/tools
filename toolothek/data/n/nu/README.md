    name        : nushell
    description : A new type of shell
    homepage    : https://www.nushell.sh
    repology    : https://repology.org/project/nushell/versions
    pkgs_vaporup: http://apt.vaporup.de/pool/main/n/nu/
