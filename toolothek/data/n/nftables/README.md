    name        : nftables
    description : control packet filtering rules by Netfilter
    homepage    : https://www.netfilter.org
    repology    : https://repology.org/project/nftables/versions
    pkgs_ubuntu : https://packages.ubuntu.com/nftables
    pkgs_debian : https://packages.debian.org/nftables
