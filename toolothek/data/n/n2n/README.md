    name        : n2n
    description : Peer-to-peer VPN
    homepage    : https://www.ntop.org/products/n2n/
    repology    : https://repology.org/project/n2n/versions
    pkgs_ubuntu : https://packages.ubuntu.com/n2n
    pkgs_debian : https://packages.debian.org/n2n
