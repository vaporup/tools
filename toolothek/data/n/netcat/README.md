    name        : netcat
    description : TCP/IP swiss army knife
    pkgs_ubuntu : https://packages.ubuntu.com/netcat
    pkgs_debian : https://packages.debian.org/netcat
