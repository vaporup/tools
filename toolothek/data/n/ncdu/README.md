    name        : ncdu
    description : ncurses disk usage viewer
    homepage    : https://dev.yorhel.nl/ncdu
    repology    : https://repology.org/project/ncdu/versions
    pkgs_ubuntu : https://packages.ubuntu.com/ncdu
    pkgs_debian : https://packages.debian.org/ncdu
