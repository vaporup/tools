    name        : netdiscover
    description : active/passive network address scanner using ARP requests
    homepage    : https://github.com/netdiscover-scanner/netdiscover
    repology    : https://repology.org/project/netdiscover/versions
    pkgs_ubuntu : https://packages.ubuntu.com/netdiscover
    pkgs_debian : https://packages.debian.org/netdiscover
