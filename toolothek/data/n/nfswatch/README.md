    name        : nfswatch
    description : monitor NFS traffic
    homepage    : https://nfswatch.sourceforge.io
    repology    : https://repology.org/project/nfswatch/versions
    pkgs_ubuntu : https://packages.ubuntu.com/nfswatch
    pkgs_debian : https://packages.debian.org/nfswatch
