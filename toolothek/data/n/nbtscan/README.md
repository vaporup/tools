    name        : nbtscan
    description : scanning networks for NetBIOS name information
    homepage    : https://github.com/resurrecting-open-source-projects/nbtscan
    repology    : https://repology.org/project/nbtscan/versions
    pkgs_ubuntu : https://packages.ubuntu.com/nbtscan
    pkgs_debian : https://packages.debian.org/nbtscan
