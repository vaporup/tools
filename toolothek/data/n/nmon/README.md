    name        : nmon
    description : performance monitoring tool
    homepage    : http://nmon.sourceforge.net
    repology    : https://repology.org/project/nmon/versions
    pkgs_ubuntu : https://packages.ubuntu.com/nmon
    pkgs_debian : https://packages.debian.org/nmon
