    name        : nvchecker
    description : new-version checker for software releases
    homepage    : https://github.com/lilydjwg/nvchecker
    repology    : https://repology.org/project/nvchecker/versions
    pkgs_ubuntu : https://packages.ubuntu.com/nvchecker
    pkgs_debian : https://packages.debian.org/nvchecker
