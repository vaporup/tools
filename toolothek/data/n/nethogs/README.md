    name        : nethogs
    description : Net top tool grouping bandwidth per process
    homepage    : https://github.com/raboof/nethogs
    repology    : https://repology.org/project/nethogs/versions
    pkgs_ubuntu : https://packages.ubuntu.com/nethogs
    pkgs_debian : https://packages.debian.org/nethogs
