    name        : nudoku
    description : text-based sudoku
    homepage    : https://github.com/jubalh/nudoku
    repology    : https://repology.org/project/nudoku/versions
    pkgs_ubuntu : https://packages.ubuntu.com/nudoku
    pkgs_debian : https://packages.debian.org/nudoku
    tag         : game
