    name        : nb
    description : web note taking, bookmarking, archiving, and knowledge base
    homepage    : https://github.com/xwmx/nb
    repology    : https://repology.org/project/nb/versions
    pkgs_vaporup: http://apt.vaporup.de/pool/main/n/nb/