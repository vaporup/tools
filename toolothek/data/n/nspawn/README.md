    name        : nspawn
    description : Pulls systemd-nspawn images from nspawn.org or some internal webserver
    homepage    : https://nspawn.org
    repology    : https://repology.org/project/nspawn/versions
    pkgs_vaporup: http://apt.vaporup.de/pool/main/n/nspawn/
