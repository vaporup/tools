    name        : nast
    description : packet sniffer and lan analyzer
    homepage    : https://www.berlios.de/software/nast/
    repology    : https://repology.org/project/nast/versions
    pkgs_ubuntu : https://packages.ubuntu.com/nast
    pkgs_debian : https://packages.debian.org/nast
