    name        : tmux
    description : terminal multiplexer
    homepage    : https://github.com/tmux/tmux/
    repology    : https://repology.org/project/tmux/versions
    pkgs_ubuntu : https://packages.ubuntu.com/tmux
    pkgs_debian : https://packages.debian.org/tmux
    tag         : terminal_multiplexer
