    name        : tcpdump
    description : command-line network traffic analyzer
    homepage    : https://www.tcpdump.org
    repology    : https://repology.org/project/tcpdump/versions
    pkgs_ubuntu : https://packages.ubuntu.com/tcpdump
    pkgs_debian : https://packages.debian.org/tcpdump
