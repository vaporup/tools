    name        : traceroute
    description : Traces the route taken by packets over an IPv4/IPv6 network
    homepage    : http://traceroute.sourceforge.net
    repology    : https://repology.org/project/traceroute/versions
    pkgs_ubuntu : https://packages.ubuntu.com/traceroute
    pkgs_debian : https://packages.debian.org/traceroute
