    name        : terminator
    description : multiple terminals in one window
    homepage    : https://gnome-terminator.org
    github      : https://github.com/gnome-terminator/terminator
    repology    : https://repology.org/project/terminator/versions
    pkgs_ubuntu : https://packages.ubuntu.com/terminator
    pkgs_debian : https://packages.debian.org/terminator
