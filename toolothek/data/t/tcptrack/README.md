    name        : tcptrack
    description : TCP connection tracker, with states and speeds
    homepage    : https://github.com/bchretien/tcptrack
    repology    : https://repology.org/project/tcptrack/versions
    pkgs_ubuntu : https://packages.ubuntu.com/tcptrack
    pkgs_debian : https://packages.debian.org/tcptrack
