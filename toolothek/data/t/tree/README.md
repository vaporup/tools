    name        : tree
    description : displays directory tree, in color
    homepage    : http://mama.indstate.edu/users/ice/tree/
    repology    : https://repology.org/project/tree-unclassified/versions
    pkgs_ubuntu : https://packages.ubuntu.com/tree
    pkgs_debian : https://packages.debian.org/tree
