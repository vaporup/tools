    name        : tig
    description : ncurses-based text-mode interface for Git
    homepage    : https://jonas.github.io/tig/
    repology    : https://repology.org/project/tig/versions
    pkgs_ubuntu : https://packages.ubuntu.com/tig
    pkgs_debian : https://packages.debian.org/tig
