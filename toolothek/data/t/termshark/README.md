    name        : termshark
    description : Terminal UI for tshark, inspired by Wireshark
    homepage    : https://termshark.io
    github      : https://github.com/gcla/termshark
    repology    : https://repology.org/project/termshark/versions
    pkgs_ubuntu : https://packages.ubuntu.com/termshark
    pkgs_debian : https://packages.debian.org/termshark
