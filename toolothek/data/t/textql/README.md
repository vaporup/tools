    name        : textql
    description : execute SQL against structured text like CSV or TSV
    homepage    : https://github.com/dinedal/textql/
    repology    : https://repology.org/project/textql/versions
    pkgs_ubuntu : https://packages.ubuntu.com/textql
    pkgs_debian : https://packages.debian.org/textql
