    name        : tcptraceroute
    description : traceroute implementation using TCP packets
    homepage    : https://github.com/mct/tcptraceroute
    repology    : https://repology.org/project/tcptraceroute/versions
    pkgs_ubuntu : https://packages.ubuntu.com/tcptraceroute
    pkgs_debian : https://packages.debian.org/tcptraceroute
