    name        : terminews
    description : read your RSS feeds from your terminal
    homepage    : https://github.com/antavelos/terminews
    repology    : https://repology.org/project/terminews/versions
    pkgs_ubuntu : https://packages.ubuntu.com/terminews
    pkgs_debian : https://packages.debian.org/terminews
    tag         : rss
