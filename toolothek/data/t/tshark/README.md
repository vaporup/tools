    name        : tshark
    description : network traffic analyzer - console version
    homepage    : https://www.wireshark.org
    repology    : https://repology.org/project/wireshark/versions
    pkgs_ubuntu : https://packages.ubuntu.com/tshark
    pkgs_debian : https://packages.debian.org/tshark
