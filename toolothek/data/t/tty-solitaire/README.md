    name        : tty-solitaire
    description : ncurses-based klondike solitaire game
    homepage    : https://github.com/mpereira/tty-solitaire
    repology    : https://repology.org/project/tty-solitaire/versions
    pkgs_ubuntu : https://packages.ubuntu.com/tty-solitaire
    pkgs_debian : https://packages.debian.org/tty-solitaire
    tag         : game
