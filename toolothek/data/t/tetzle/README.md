    name        : tetzle
    description : Jigsaw puzzle game
    homepage    : https://gottcode.org/tetzle/
    repology    : https://repology.org/project/tetzle/versions
    pkgs_ubuntu : https://packages.ubuntu.com/tetzle
    pkgs_debian : https://packages.debian.org/tetzle
    video       : https://www.youtube.com/watch?v=VYo6clsSIfU
    tag         : game
    tag         : puzzle
