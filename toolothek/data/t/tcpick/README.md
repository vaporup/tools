    name        : tcpick
    description : TCP stream sniffer and connection tracker
    homepage    : http://tcpick.sourceforge.net
    repology    : https://repology.org/project/tcpick/versions
    pkgs_ubuntu : https://packages.ubuntu.com/tcpick
    pkgs_debian : https://packages.debian.org/tcpick
