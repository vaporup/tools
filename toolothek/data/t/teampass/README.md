    name        : teampass
    description : collaborative passwords manager
    homepage    : https://teampass.net
    repology    : https://repology.org/project/teampass/versions
    tag         : php
