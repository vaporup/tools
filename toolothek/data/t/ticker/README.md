    name        : ticker
    description : configurable text scroller
    homepage    : http://joeyh.name/code/ticker/
    repology    : https://repology.org/project/ticker-joey/versions
    pkgs_ubuntu : https://packages.ubuntu.com/ticker
    pkgs_debian : https://packages.debian.org/ticker
