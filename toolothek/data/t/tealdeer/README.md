    name        : tealdeer
    description : very fast implementation of tldr in Rust
    homepage    : https://dbrgn.github.io/tealdeer/
    repology    : https://repology.org/project/tealdeer/versions
    github      : https://github.com/dbrgn/tealdeer
    pkgs_vaporup: http://apt.vaporup.de/pool/main/t/tealdeer/
