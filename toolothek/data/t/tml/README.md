    name        : tml
    description : make the output of coloured/formatted text in the terminal
    homepage    : https://github.com/liamg/tml
    repology    : https://repology.org/project/tml/versions
    pkgs_ubuntu : https://packages.ubuntu.com/tml
    pkgs_debian : https://packages.debian.org/tml
