    name        : toilet
    description : display large colourful characters in text mode
    homepage    : http://caca.zoy.org/wiki/toilet
    repology    : https://repology.org/project/toilet/versions
    pkgs_ubuntu : https://packages.ubuntu.com/toilet
    pkgs_debian : https://packages.debian.org/toilet
