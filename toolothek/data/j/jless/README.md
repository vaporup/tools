    name        : jless
    description : pager for JSON or YAML data
    homepage    : https://jless.io
    repology    : https://repology.org/project/jless/versions
    pkgs_vaporup: http://apt.vaporup.de/pool/main/j/jless/
