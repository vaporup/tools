    name        : jshon
    description : Command line tool to parse, read and create JSON
    homepage    : http://kmkeen.com/jshon/
    repology    : https://repology.org/project/jshon/versions
    pkgs_ubuntu : https://packages.ubuntu.com/jshon
    pkgs_debian : https://packages.debian.org/jshon
