    name        : jello
    description : filter JSON and JSON Lines data with Python syntax. (Similar to jq)
    homepage    : https://kellyjonbrazil.github.io/jello/
    github      : https://github.com/kellyjonbrazil/jello
    repology    : https://repology.org/project/jello/versions
    pkgs_ubuntu : https://packages.ubuntu.com/jello
    pkgs_debian : https://packages.debian.org/jello
