    name        : jq
    description : lightweight and flexible command-line JSON processor
    homepage    : https://github.com/stedolan/jq
    repology    : https://repology.org/project/jq/versions
    pkgs_ubuntu : https://packages.ubuntu.com/jq
    pkgs_debian : https://packages.debian.org/jq
