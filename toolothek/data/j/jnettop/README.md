    name        : jnettop
    description : hosts/ports taking up the most network traffic
    homepage    : https://web.archive.org/web/20160703195221/http://jnettop.kubs.info/wiki/
    repology    : https://repology.org/project/jnettop/versions
    pkgs_ubuntu : https://packages.ubuntu.com/jnettop
    pkgs_debian : https://packages.debian.org/jnettop
