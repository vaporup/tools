    name        : jailer
    description : database subsetting, schema and data browsing
    homepage    : http://jailer.sourceforge.net
    repology    : https://repology.org/project/jailer/versions
