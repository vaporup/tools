    name        : jami
    description : Secure and distributed voice, video, and chat platform
    homepage    : https://jami.net
    repology    : https://repology.org/project/jami/versions
    pkgs_ubuntu : https://packages.ubuntu.com/jami
    pkgs_debian : https://packages.debian.org/jami
