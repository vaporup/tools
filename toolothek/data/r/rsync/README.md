    name        : rsync
    description : fast, versatile file-copying tool
    homepage    : https://rsync.samba.org
    repology    : https://repology.org/project/rsync/versions
    pkgs_ubuntu : https://packages.ubuntu.com/rsync
    pkgs_debian : https://packages.debian.org/rsync
