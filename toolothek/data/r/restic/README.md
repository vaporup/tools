    name        : restic
    description : backup program with multiple revisions, encryption and more
    homepage    : https://github.com/restic/restic
    repology    : https://repology.org/project/restic/versions
    pkgs_ubuntu : https://packages.ubuntu.com/restic
    pkgs_debian : https://packages.debian.org/restic
    tag         : backup
