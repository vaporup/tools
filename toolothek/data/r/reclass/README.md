    name        : reclass
    description : hierarchical inventory backend for configuration management systems
    homepage    : https://github.com/salt-formulas/reclass
    repology    : https://repology.org/project/reclass/versions
    pkgs_ubuntu : https://packages.ubuntu.com/reclass
    pkgs_debian : https://packages.debian.org/reclass
