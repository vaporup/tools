    name        : rsync-sidekick
    description : designed to run before rsync
    homepage    : https://github.com/m-manu/rsync-sidekick
    pkgs_vaporup: http://apt.vaporup.de/pool/main/r/rsync-sidekick/
