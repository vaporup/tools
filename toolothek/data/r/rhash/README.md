    name        : rhash
    description : computing hash sums and magnet links
    homepage    : http://rhash.sourceforge.net
    repology    : https://repology.org/project/rhash/versions
    pkgs_ubuntu : https://packages.ubuntu.com/rhash
    pkgs_debian : https://packages.debian.org/rhash
