    name        : rlwrap
    description : readline feature command line wrapper
    homepage    : https://github.com/hanslub42/rlwrap
    repology    : https://repology.org/project/rlwrap/versions
    pkgs_ubuntu : https://packages.ubuntu.com/rlwrap
    pkgs_debian : https://packages.debian.org/rlwrap
