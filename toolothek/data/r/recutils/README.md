    name        : recutils
    description : text-based databases called recfiles
    homepage    : https://www.gnu.org/software/recutils/
    repology    : https://repology.org/project/recutils/versions
    pkgs_ubuntu : https://packages.ubuntu.com/recutils
    pkgs_debian : https://packages.debian.org/recutils
