    name        : rdiff
    description : binary diff tool for signature-based differences
    homepage    : https://github.com/librsync/librsync
    pkgs_ubuntu : https://packages.ubuntu.com/rdiff
    pkgs_debian : https://packages.debian.org/rdiff
