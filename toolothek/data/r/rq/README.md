    name        : rq
    description : record analysis and transformation with JSON,YAML, etc
    homepage    : https://github.com/dflemstr/rq
    repology    : https://repology.org/project/rq/versions
