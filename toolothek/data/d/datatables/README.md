    name        : datatables
    description : makes nice tables from different data sources
    homepage    : https://datatables.net
    pkgs_ubuntu : https://packages.ubuntu.com/libjs-jquery-datatables
    pkgs_ubuntu : https://packages.ubuntu.com/libjs-jquery-datatables-extensions
    pkgs_debian : https://packages.debian.org/libjs-jquery-datatables
    pkgs_debian : https://packages.debian.org/libjs-jquery-datatables-extensions
