    name        : dive
    description : A tool for exploring each layer in a docker image
    homepage    : https://github.com/wagoodman/dive
