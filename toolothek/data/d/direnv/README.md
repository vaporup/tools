    name        : direnv
    description : set directory specific environment variables
    homepage    : https://direnv.net
    pkgs_ubuntu : https://packages.ubuntu.com/direnv
    pkgs_debian : https://packages.debian.org/direnv
