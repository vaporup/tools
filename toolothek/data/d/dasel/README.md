    name        : dasel
    description : One tool to rule them all (jq, yq, etc.)
    homepage    : https://github.com/TomWright/dasel
    pkgs_vaporup: http://apt.vaporup.de/pool/main/d/dasel/
