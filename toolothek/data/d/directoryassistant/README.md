    name        : directoryassistant
    description : small LDAP address book manager
    homepage    : https://olivier.sessink.nl/directoryassistant/
    pkgs_ubuntu : https://packages.ubuntu.com/directoryassistant
    pkgs_debian : https://packages.debian.org/directoryassistant
