    name        : debootstrap
    description : Bootstrap a basic Debian system
    homepage    : https://salsa.debian.org/installer-team/debootstrap
    pkgs_ubuntu : https://packages.ubuntu.com/debootstrap
    pkgs_debian : https://packages.debian.org/debootstrap
