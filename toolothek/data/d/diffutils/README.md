    name        : diffutils
    description : file comparison utilities
    homepage    : https://www.gnu.org/software/diffutils/
    pkgs_ubuntu : https://packages.ubuntu.com/diffutils
    pkgs_debian : https://packages.debian.org/diffutils
    tag         : diff
