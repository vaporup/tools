    name        : dstat
    description : versatile resource statistics tool
    homepage    : http://dag.wiee.rs/home-made/dstat/
    pkgs_ubuntu : https://packages.ubuntu.com/dstat
    pkgs_debian : https://packages.debian.org/dstat
