    name        : due
    description : manage build environments in Docker containers
    homepage    : https://github.com/CumulusNetworks/DUE
    pkgs_ubuntu : https://packages.ubuntu.com/due
    pkgs_debian : https://packages.debian.org/due
