    name        : debspawn
    description : Build deb packages in nspawn containers
    homepage    : https://github.com/lkhq/debspawn
    pkgs_ubuntu : https://packages.ubuntu.com/debspawn
    pkgs_debian : https://packages.debian.org/debspawn
