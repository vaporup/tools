
    name        : debtags
    description : a set of categories to describe Debian packages
    homepage    : https://wiki.debian.org/Debtags
    repology    : https://repology.org/project/debtags/versions
    pkgs_ubuntu : http://packages.ubuntu.com/debtags
    pkgs_debian : http://packages.debian.org/debtags

![](logo.png)

[![Packaging status](https://repology.org/badge/vertical-allrepos/debtags.svg?columns=4)](https://repology.org/project/debtags/versions)
