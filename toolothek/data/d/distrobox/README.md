    name        : distrobox
    description : Use any linux distribution inside your terminal
    homepage    : https://distrobox.it
    github      : https://github.com/89luca89/distrobox
    pkgs_ubuntu : https://packages.ubuntu.com/distrobox
    pkgs_debian : https://packages.debian.org/distrobox
    pkgs_vaporup: http://apt.vaporup.de/pool/main/d/distrobox/
    info        : https://linuxnews.de/distrobox-distributionen-im-terminal-testen/
    info        : https://linuxconfig.org/how-to-integrate-any-linux-distribution-inside-a-terminal-with-distrobox
