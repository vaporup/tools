    name        : dnstop
    description : analyze DNS traffic
    homepage    : http://dns.measurement-factory.com/tools/dnstop/
    pkgs_ubuntu : https://packages.ubuntu.com/dnstop
    pkgs_debian : https://packages.debian.org/dnstop
