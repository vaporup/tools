    name        : dmidecode
    description : SMBIOS/DMI table decoder
    homepage    : https://www.nongnu.org/dmidecode/
    pkgs_ubuntu : https://packages.ubuntu.com/dmidecode
    pkgs_debian : https://packages.debian.org/dmidecode
