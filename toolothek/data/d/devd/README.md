    name        : devd
    description : local webserver for developers
    homepage    : https://github.com/cortesi/devd
    pkgs_vaporup: http://apt.vaporup.de/pool/main/d/devd/
