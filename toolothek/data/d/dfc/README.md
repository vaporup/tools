    name        : dfc
    description : display file system usage using graph and colors
    homepage    : https://projects.gw-computing.net/projects/dfc
    pkgs_ubuntu : https://packages.ubuntu.com/dfc
    pkgs_debian : https://packages.debian.org/dfc
