    name        : devede
    description : create Video DVDs
    homepage    : https://www.rastersoft.com/programas/devede.html
    pkgs_ubuntu : https://packages.ubuntu.com/devede
    pkgs_debian : https://packages.debian.org/devede
