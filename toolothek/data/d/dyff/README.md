    name        : dyff
    description : diff tool for YAML files, and sometimes JSON
    homepage    : https://github.com/homeport/dyff
    pkgs_vaporup: http://apt.vaporup.de/pool/main/d/dyff/
    tag         : diff
