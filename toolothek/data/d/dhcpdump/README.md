    name        : dhcpdump
    description : Parse DHCP packets from tcpdump
    pkgs_ubuntu : https://packages.ubuntu.com/dhcpdump
    pkgs_debian : https://packages.debian.org/dhcpdump
