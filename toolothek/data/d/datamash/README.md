    name        : datamash
    description : statistical operations on input textual data files
    homepage    : https://savannah.gnu.org/projects/datamash/
    pkgs_ubuntu : https://packages.ubuntu.com/datamash
    pkgs_debian : https://packages.debian.org/datamash
