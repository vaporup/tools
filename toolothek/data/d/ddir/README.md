    name        : ddir
    description : display hierarchical directory tree
    homepage    : https://github.com/jaalto/project--perl-ddir
    pkgs_ubuntu : https://packages.ubuntu.com/ddir
    pkgs_debian : https://packages.debian.org/ddir
