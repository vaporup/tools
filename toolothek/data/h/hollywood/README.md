    name        : hollywood
    description : fill your console with Hollywood melodrama technobabble
    homepage    : https://a.hollywood.computer
    repology    : https://repology.org/project/hollywood/versions
    pkgs_ubuntu : https://packages.ubuntu.com/hollywood
    pkgs_debian : https://packages.debian.org/hollywood
