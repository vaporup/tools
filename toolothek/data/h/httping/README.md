    name        : httping
    description : ping-like program for http-requests
    homepage    : https://www.vanheusden.com/httping/
    repology    : https://repology.org/project/httping/versions
    pkgs_ubuntu : https://packages.ubuntu.com/httping
    pkgs_debian : https://packages.debian.org/httping
