    name        : handbrake
    description : DVD ripper and video transcoder
    homepage    : https://handbrake.fr
    repology    : https://repology.org/project/handbrake/versions
    pkgs_ubuntu : https://packages.ubuntu.com/handbrake
    pkgs_debian : https://packages.debian.org/handbrake
