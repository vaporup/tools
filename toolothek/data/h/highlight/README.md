    name        : highlight
    description : source code to formatted text converter
    homepage    : http://www.andre-simon.de/doku/highlight/en/highlight.php
    repology    : https://repology.org/project/highlight/versions
    pkgs_ubuntu : https://packages.ubuntu.com/highlight
    pkgs_debian : https://packages.debian.org/highlight
