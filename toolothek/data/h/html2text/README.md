    name        : html2text
    description : HTML to text converter
    homepage    : https://github.com/grobian/html2text
    repology    : https://repology.org/project/html2text/versions
    pkgs_ubuntu : https://packages.ubuntu.com/html2text
    pkgs_debian : https://packages.debian.org/html2text
