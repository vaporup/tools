    name        : hugo
    description : Fast and flexible Static Site Generator written in Go
    homepage    : https://gohugo.io
    repology    : https://repology.org/project/hugo-sitegen/versions
    pkgs_ubuntu : https://packages.ubuntu.com/hugo
    pkgs_debian : https://packages.debian.org/hugo
