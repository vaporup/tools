    name        : htop
    description : interactive processes viewer
    homepage    : https://htop.dev
    repology    : https://repology.org/project/htop/versions
    pkgs_ubuntu : https://packages.ubuntu.com/htop
    pkgs_debian : https://packages.debian.org/htop
