    name        : httest
    description : testing and benchmarking web applications, servers, proxies and browsers
    homepage    : https://sourceforge.net/projects/htt/
    repology    : https://repology.org/project/httest/versions
    pkgs_ubuntu : https://packages.ubuntu.com/httest
    pkgs_debian : https://packages.debian.org/httest
    info        : https://sourceforge.net/p/htt/wiki/Home/
