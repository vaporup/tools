    name        : hurl
    description : command line tool that runs HTTP requests defined in plain text format
    homepage    : https://hurl.dev
    repology    : https://repology.org/project/hurl-http-requester/versions
    pkgs_vaporup: http://apt.vaporup.de/pool/main/h/hurl/
