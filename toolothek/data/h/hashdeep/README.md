    name        : hashdeep
    description : recursively compute hashsums or piecewise hashings
    homepage    : http://md5deep.sourceforge.net
    repology    : https://repology.org/project/md5deep/versions
    pkgs_ubuntu : https://packages.ubuntu.com/hashdeep
    pkgs_debian : https://packages.debian.org/hashdeep
