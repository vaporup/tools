    name        : hstr
    description : Suggest box like shell history completion
    homepage    : https://github.com/dvorka/hstr
    repology    : https://repology.org/project/hstr/versions
    pkgs_ubuntu : https://packages.ubuntu.com/hstr
    pkgs_debian : https://packages.debian.org/hstr
