    name        : hwatch
    description : modern alternative to the watch command
    homepage    : https://github.com/blacknon/hwatch
    repology    : https://repology.org/project/hwatch/versions
    pkgs_vaporup: http://apt.vaporup.de/pool/main/h/hwatch/
