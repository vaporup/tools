    name        : help2man
    description : Automatic manpage generator
    homepage    : https://www.gnu.org/software/help2man/
    repology    : https://repology.org/project/help2man/versions
    pkgs_ubuntu : https://packages.ubuntu.com/help2man
    pkgs_debian : https://packages.debian.org/help2man
