    name        : httpie
    description : cURL-like tool for humans
    homepage    : https://httpie.io
    repology    : https://repology.org/project/httpie/versions
    pkgs_ubuntu : https://packages.ubuntu.com/httpie
    pkgs_debian : https://packages.debian.org/httpie
