    name        : lsof
    description : list open files
    homepage    : https://github.com/lsof-org/lsof
    repology    : https://repology.org/project/lsof/versions
    pkgs_ubuntu : https://packages.ubuntu.com/lsof
    pkgs_debian : https://packages.debian.org/lsof
