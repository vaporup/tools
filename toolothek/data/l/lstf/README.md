    name        : lstf
    description : aggregated TCP flows printer
    homepage    : https://github.com/yuuki/lstf
    pkgs_vaporup: http://apt.vaporup.de/pool/main/l/lstf/
