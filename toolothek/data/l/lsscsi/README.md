    name        : lsscsi
    description : list all SCSI devices (or hosts) currently on system
    homepage    : http://sg.danny.cz/scsi/lsscsi.html
    repology    : https://repology.org/project/lsscsi/versions
    pkgs_ubuntu : https://packages.ubuntu.com/lsscsi
    pkgs_debian : https://packages.debian.org/lsscsi
