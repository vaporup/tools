    name        : lldpd
    description : supports several discovery protocols (LLDP,CDP,...)
    homepage    : https://lldpd.github.io
    repology    : https://repology.org/project/lldpd/versions
    pkgs_ubuntu : https://packages.ubuntu.com/lldpd
    pkgs_debian : https://packages.debian.org/lldpd

# Example

```
lldpcli show neighbors detail
```
