    name        : lnav
    description : ncurses-based log file viewer
    homepage    : https://lnav.org
    repology    : https://repology.org/project/lnav/versions
    pkgs_ubuntu : https://packages.ubuntu.com/lnav
    pkgs_debian : https://packages.debian.org/lnav
    pkgs_vaporup: http://apt.vaporup.de/pool/main/l/lnav/
    info        : https://lnav.org/2021/05/03/tailing-remote-files.html
    info        : https://lnav.org/blog/
