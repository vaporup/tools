    name        : ldap-git-backup
    description : Back up LDAP database in an Git repository
    homepage    : https://github.com/elmar/ldap-git-backup
    repology    : https://repology.org/project/ldap-git-backup/versions
    pkgs_ubuntu : https://packages.ubuntu.com/ldap-git-backup
    pkgs_debian : https://packages.debian.org/ldap-git-backup
