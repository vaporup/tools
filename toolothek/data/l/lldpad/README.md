    name        : lldpad
    description : manage an LLDP interface with support for TLVs
    homepage    : https://github.com/intel/openlldp
    repology    : https://repology.org/project/lldpad/versions
    pkgs_ubuntu : https://packages.ubuntu.com/lldpad
    pkgs_debian : https://packages.debian.org/lldpad
