    name        : litestore
    description : tiny, lightweight, self-contained, RESTful document store
    homepage    : https://h3rald.com/litestore/
