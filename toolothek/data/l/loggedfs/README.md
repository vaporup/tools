    name        : loggedfs
    description : filesystem monitoring with Fuse
    homepage    : https://rflament.github.io/loggedfs/
    repology    : https://repology.org/project/loggedfs/versions
    pkgs_ubuntu : https://packages.ubuntu.com/loggedfs
    pkgs_debian : https://packages.debian.org/loggedfs
    tag         : monitoring

[![Packaging status](https://repology.org/badge/vertical-allrepos/loggedfs.svg?columns=4)](https://repology.org/project/loggedfs/versions)
