    name        : lolcat
    description : colorful cat
    homepage    : https://github.com/busyloop/lolcat
    repology    : https://repology.org/project/lolcat-busyloop/versions
    pkgs_ubuntu : https://packages.ubuntu.com/lolcat
    pkgs_debian : https://packages.debian.org/lolcat
