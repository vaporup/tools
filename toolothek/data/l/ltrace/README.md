    name        : ltrace
    description : Tracks runtime library calls in dynamically linked programs
    repology    : https://repology.org/project/ltrace/versions
    pkgs_ubuntu : https://packages.ubuntu.com/ltrace
    pkgs_debian : https://packages.debian.org/ltrace
