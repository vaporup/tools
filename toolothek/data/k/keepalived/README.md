    name        : keepalived
    description : Failover and monitoring daemon for LVS clusters
    homepage    : https://keepalived.org
    repology    : https://repology.org/project/keepalived/versions
    pkgs_ubuntu : https://packages.ubuntu.com/keepalived
    pkgs_debian : https://packages.debian.org/keepalived
    info        : https://tobru.ch/keepalived-check-and-notify-scripts/
