    name        : kdenlive
    description : non-linear video editor
    homepage    : https://kdenlive.org
    repology    : https://repology.org/project/kdenlive/versions
    pkgs_ubuntu : https://packages.ubuntu.com/kdenlive
    pkgs_debian : https://packages.debian.org/kdenlive
