    name        : kwalify
    description : parser, schema validator, and data-binding tool for YAML and JSON
    homepage    : http://www.kuwata-lab.com/kwalify/
    repology    : https://repology.org/project/kwalify/versions
    pkgs_ubuntu : https://packages.ubuntu.com/kwalify
    pkgs_debian : https://packages.debian.org/kwalify
