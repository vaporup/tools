module clickTT

go 1.18

require (
	github.com/anaskhan96/soup v1.2.5
	github.com/galdor/go-cmdline v1.2.0
	golang.org/x/net v0.0.0-20221004154528-8021a29435af // indirect
	sigs.k8s.io/yaml v1.4.0
)

require golang.org/x/text v0.3.7 // indirect
