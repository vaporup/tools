package main

import (
	"bytes"
	"fmt"
	"io"
	"net"
	"net/http"
	"strings"

	"github.com/labstack/echo/v4"
)

const (
	SERVER_HOST = "127.0.0.1"
	SERVER_PORT = "6557"
	SERVER_TYPE = "tcp"
)

func main() {

	e := echo.New()
        e.Debug = true

	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "checkmk-lql-http")
	})

	e.POST("/", func(c echo.Context) error {

		file, err := c.FormFile("file")

		if err != nil {
			return err
		}

		src, err := file.Open()

		if err != nil {
			return err
		}

		defer src.Close()

		content, err := io.ReadAll(src)

		if err != nil {
			return err
		}

		query := []string{}

		found_output_format := false

		lines := strings.Split(string(content), "\n")

		for _, line := range lines {

			line = strings.Replace(line, "\n", "", -1)
			line = strings.Replace(line, "\r", "", -1)
			line = strings.TrimSpace(line)

			if line == "" {
				continue
			}

			if !strings.Contains(strings.ToLower(line), "outputformat") {
				query = append(query, line)
				continue
			}

			if strings.Contains(strings.ToLower(line), "outputformat") && !found_output_format {
				query = append(query, line)
				found_output_format = true
				continue
			}

		}

		if !found_output_format {
			query = append(query, "OutputFormat: json")
			c.Response().Header().Set(
				echo.HeaderContentType,
				echo.MIMEApplicationJSONCharsetUTF8)

		}

		query = append(query, "\n")
		lql := strings.Join(query, "\n")

		conn, err := net.Dial(SERVER_TYPE, SERVER_HOST+":"+SERVER_PORT)

		if err != nil {
			fmt.Println("dial error:", err)
			return c.String(http.StatusOK, fmt.Sprintf("%s", err))
		}

		defer conn.Close()

		fmt.Fprintf(conn, lql)

		var buf bytes.Buffer
		io.Copy(&buf, conn)

		return c.String(http.StatusOK, buf.String())

	})

	e.Logger.Fatal(e.Start(":1323"))

}
