
> query.lql
```
GET hosts
Columns: name
Limit: 2
```


    curl -X POST --form file="@query.lql" http://127.0.0.1:1323/

> /etc/systemd/system/checkmk-lql-http.service

```
[Unit]
Description=CheckMK Livestatus to HTTP Bridge (swick)
After=network.target

[Service]
Type=simple
User=root
Group=root

ExecStart=/usr/local/bin/checkmk-lql-http

[Install]
WantedBy=multi-user.target
```

    systemctl enable checkmk-lql-http.service

> Apache

```
                ProxyPass               /lql    http://127.0.0.1:1323/
                ProxyPassReverse        /lql    http://127.0.0.1:1323/

```
