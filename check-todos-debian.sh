#!/usr/bin/env bash

for tool in $(grep 'https:/' toolothek/todo.txt | awk '{print $1}' | awk -F'/' '{print $NF}'); do
  sleep 10
  curl -s "https://packages.debian.org/stable/$tool" \
  | grep -q 'Package not available in this suite'    \
  && echo "https://packages.debian.org/$tool"
done
